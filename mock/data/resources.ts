import { ESpecNetworkUsageState } from '../../rest_client/saas/model/eSpecNetworkUsageState';
import { ESpecNetwork } from '../../rest_client/saas/model/eSpecNetwork';

export const resources: ESpecNetwork[] = [
  {
    identifier: '1',
    name: 'prod-vpc',
    aws: {
      region: 'us-east-1',
    },
    usageState: ESpecNetworkUsageState.Discovered
  },
  {
    identifier: '2',
    name: 'dev-vpc',
    aws: {
      region: 'us-east-1',
    },
    usageState: ESpecNetworkUsageState.Discovered
  },
  {
    identifier: '3',
    name: 'engr-vnet',
    azure: {
      region: 'West Europe',
    },
    usageState: ESpecNetworkUsageState.Discovered
  },
  {
    identifier: '4',
    name: 'qa-vnet',
    azure: {
      region: 'West Europe',
    },
    usageState: ESpecNetworkUsageState.Discovered
  },
  {
    identifier: '5',
    name: 'starlight-apps-prod',
    gcp: {
      providerVpcId: 'asia-east1-a'
    },
    usageState: ESpecNetworkUsageState.Discovered
  },
  {
    identifier: '6',
    name: 'starlight-dmz-dev',
    gcp: {
      providerVpcId: 'asia-east1-a'
    },
    usageState: ESpecNetworkUsageState.Discovered
  },
  {
    identifier: '7',
    name: 'starlight-db-test',
    gcp: {
      providerVpcId: 'asia-west1-a'
    },
    usageState: ESpecNetworkUsageState.Discovered
  }
];