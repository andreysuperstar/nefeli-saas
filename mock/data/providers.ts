import { Provider } from '../../rest_client/saas/model/provider';
import { Providers as RESTProviders } from '../../rest_client/saas/model/providers';
import { Request, Response } from 'express';

export const AwsProvider: Provider = {
  aws: {},
  name: 'AWS Provider',
  description: 'Provider for Amazon Web Services',
  identifier: '1'
};
export const AzureProvider: Provider = {
  azure: {},
  name: 'Azure Provider',
  description: 'Provider for Azure',
  identifier: '2'
};

export const Providers: RESTProviders = {
  providers: [
    AwsProvider,
    AzureProvider
  ]
};

export const FindProviders = (req: Request, res: Response) => {
  const status = 200;
  const id = req.params['id'];
  if (id) {
    res.status(status).jsonp(Providers.providers?.filter(p => p.identifier === id));
    return;
  }
  const filter: string = req.query['filter'] as string;
  if (!filter) {
    res.status(status).jsonp(Providers);
    return;
  }
  const providers = Providers.providers?.filter(p => p[filter as keyof Provider]);
  res.status(status).jsonp({ providers });
};