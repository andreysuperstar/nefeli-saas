import { ExternalNetworkSpecStatus } from '../../rest_client/saas/model/externalNetworkSpecStatus';
import { ESpecLink } from '../../rest_client/saas/model/eSpecLink';
import { ESpecLogicalRouter } from '../../rest_client/saas/model/eSpecLogicalRouter';
import { ExternalNetworkSpec } from '../../rest_client/saas/model/externalNetworkSpec';
import { ExternalNetworkSpecConfig } from '../../rest_client/saas/model/externalNetworkSpecConfig';
import { Networks } from './networks';
import { Request, Response } from 'express';
import { ExternalNetworkSpecStatusResourceDeployStatusLifecycleState } from '../../rest_client/saas/model/externalNetworkSpecStatusResourceDeployStatusLifecycleState';

const routers: ESpecLogicalRouter[] = [
  {
    identifier: '1',
    name: 'Router'
  }
];

const links: ESpecLink[] = [
  {
    identifier: '1',
    nodeA: {
      networkIdentifier: '1',
      logicalRouterIdentifier: ''
    },
    nodeB: {
      networkIdentifier: '',
      logicalRouterIdentifier: '1'
    }
  },
  {
    identifier: '2',
    nodeA: {
      networkIdentifier: '',
      logicalRouterIdentifier: '1'
    },
    nodeB: {
      networkIdentifier: '2',
      logicalRouterIdentifier: ''
    }
  },
  {
    identifier: '3',
    nodeA: {
      networkIdentifier: '',
      logicalRouterIdentifier: '1'
    },
    nodeB: {
      networkIdentifier: '3',
      logicalRouterIdentifier: ''
    }
  },
  {
    identifier: '4',
    nodeA: {
      networkIdentifier: '3',
      logicalRouterIdentifier: ''
    },
    nodeB: {
      networkIdentifier: '4',
      logicalRouterIdentifier: ''
    }
  }
];

const config: ExternalNetworkSpecConfig = {
  identifier: 'netspec-id',
  name: 'Netspec #1',
  description: 'The description for this netspec',
  links,
  networkIds: Networks.networks?.map(network => network.identifier || ''),
  routers
};

const getStatus = (count = 0): ExternalNetworkSpecStatus => {
  return {
    resourceCosts: {
      one: {
        monthlyEstimate: {
          cost: 109.50,
          currency: 'USD',
          description: 'Amazon (AWS)'
        },
        prices: [
          {
            currency: 'USD',
            unit: 'VPC Peering (same region)',
            unitPrice: 3.99
          },
          {
            currency: 'USD',
            unit: 'S3 5GB',
            unitPrice: 2.50
          },
          {
            currency: 'USD',
            unit: 'EC2 instance',
            unitPrice: 275.10
          }
        ],
        pricesAt: `${new Date().getTime()}`
      },
      two: {
        monthlyEstimate: {
          cost: 1022.20,
          currency: 'USD',
          description: 'Microsoft (Azure)'
        },
        prices: [
          {
            currency: 'USD',
            unit: 'Basic VPN Gateway',
            unitPrice: 26.28
          },
          {
            currency: 'USD',
            unit: 'Route Server',
            unitPrice: 2.50
          },
          {
            currency: 'USD',
            unit: 'Content Delivery Network',
            unitPrice: 275.10
          }
        ],
        pricesAt: `${new Date().getTime()}`
      },
      three: {
        monthlyEstimate: {
          cost: 26.28,
          currency: 'USD',
          description: 'Google Cloud Platform'
        },
        prices: [
          {
            currency: 'USD',
            unit: 'Transit Gateway',
            unitPrice: 109.50
          }
        ],
        pricesAt: `${new Date().getTime()}`
      }
    },
    resourcesByNetspecElement: {
      1: {
        concreteCloudResourceIds: ['1', '2']
      },
      2: {
        concreteCloudResourceIds: ['3', '4']
      },
      3: {
        concreteCloudResourceIds: ['5', '6']
      },
      4: {
        concreteCloudResourceIds: ['7', '8', '9']
      }
    },
    resourceDeployStatuses: {
      1: {
        state: ExternalNetworkSpecStatusResourceDeployStatusLifecycleState.Deployed
      },
      2: {
        state: count < 5
          ? ExternalNetworkSpecStatusResourceDeployStatusLifecycleState.PendingCreation
          : ExternalNetworkSpecStatusResourceDeployStatusLifecycleState.Deployed
      },
      3: {
        state: count < 7
          ? ExternalNetworkSpecStatusResourceDeployStatusLifecycleState.Creating
          : ExternalNetworkSpecStatusResourceDeployStatusLifecycleState.Deployed
      },
      4: {
        state: ExternalNetworkSpecStatusResourceDeployStatusLifecycleState.Deployed
      },
      5: {
        state: ExternalNetworkSpecStatusResourceDeployStatusLifecycleState.Deployed
      },
      6: {
        state: ExternalNetworkSpecStatusResourceDeployStatusLifecycleState.Deployed
      },
      7: {
        state: ExternalNetworkSpecStatusResourceDeployStatusLifecycleState.Deployed
      },
      8: {
        state: ExternalNetworkSpecStatusResourceDeployStatusLifecycleState.Creating
      },
      9: {
        state: count < 10
          ? ExternalNetworkSpecStatusResourceDeployStatusLifecycleState.PendingCreation
          : ExternalNetworkSpecStatusResourceDeployStatusLifecycleState.RequiresAttention
      }
    }
  }
}

export const NetspecConfig: ExternalNetworkSpecConfig = config;
export const Netspec: ExternalNetworkSpec = { config, status: getStatus() };

let count = 0;
export const NetspecById = (req: Request, res: Response) => {
  const netspec: ExternalNetworkSpec = { config, status: getStatus(count++) };
  res.status(200).jsonp(netspec);
};