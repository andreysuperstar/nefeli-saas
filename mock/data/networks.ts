import { ESpecNetwork } from '../../rest_client/saas/model/eSpecNetwork';
import { ESpecNetworks } from '../../rest_client/saas/model/eSpecNetworks';
import { Request, Response } from 'express';

export const Networks: ESpecNetworks = {
  networks: [
    {
      identifier: '1',
      name: 'prod-vpc',
      aws: {
        region: 'us-east-1'
      }
    },
    {
      identifier: '2',
      name: 'dev-vpc',
      aws: {
        region: 'us-west-1'
      }
    },
    {
      identifier: '3',
      name: 'qa-vnet',
      azure: {
        region: 'West Europe'
      }
    },
    {
      identifier: '4',
      name: 'engr-vnet',
      azure: {
        region: 'West Europe'
      }
    }
  ] as Array<ESpecNetwork>
};

export const NetworksById = (req: Request, res: Response) => {
  const lookupId = req.params['id'];
  const okResponse = 200;
  res.status(okResponse).jsonp(getNetworkById(lookupId));
};

export const getNetworkById = (id: string): ESpecNetwork | undefined => {
  return Networks.networks?.find(({ identifier }: ESpecNetwork) => identifier === id)
}