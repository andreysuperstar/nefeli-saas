"use strict";
exports.__esModule = true;
exports.Netspec = exports.NetspecConfig = void 0;
var networks_1 = require("./networks");
var routers = [
    {
        identifier: '1',
        name: 'Router'
    }
];
var links = [
    {
        identifier: '1',
        nodeA: {
            networkIdentifier: '1',
            logicalRouterIdentifier: ''
        },
        nodeB: {
            networkIdentifier: '',
            logicalRouterIdentifier: '1'
        }
    },
    {
        identifier: '2',
        nodeA: {
            networkIdentifier: '',
            logicalRouterIdentifier: '1'
        },
        nodeB: {
            networkIdentifier: '2',
            logicalRouterIdentifier: ''
        }
    },
    {
        identifier: '3',
        nodeA: {
            networkIdentifier: '',
            logicalRouterIdentifier: '1'
        },
        nodeB: {
            networkIdentifier: '3',
            logicalRouterIdentifier: ''
        }
    },
    {
        identifier: '4',
        nodeA: {
            networkIdentifier: '3',
            logicalRouterIdentifier: ''
        },
        nodeB: {
            networkIdentifier: '4',
            logicalRouterIdentifier: ''
        }
    }
];
var config = {
    identifier: 'netspec-id',
    name: 'Netspec #1',
    description: 'The description for this netspec',
    links: links,
    networkIds: Object.keys(networks_1.Networks),
    routers: routers
};
var status = {
    resourceCosts: {
        one: {
            monthlyEstimate: {
                cost: 109.50,
                currency: 'USD',
                description: 'Amazon (AWS)'
            },
            prices: [
                {
                    currency: 'USD',
                    unit: 'VPC Peering (same region)',
                    unitPrice: 3.99
                },
                {
                    currency: 'USD',
                    unit: 'S3 5GB',
                    unitPrice: 2.50
                },
                {
                    currency: 'USD',
                    unit: 'EC2 instance',
                    unitPrice: 275.10
                }
            ],
            pricesAt: "".concat(new Date().getTime())
        },
        two: {
            monthlyEstimate: {
                cost: 1022.20,
                currency: 'USD',
                description: 'Microsoft (Azure)'
            },
            prices: [
                {
                    currency: 'USD',
                    unit: 'Basic VPN Gateway',
                    unitPrice: 26.28
                },
                {
                    currency: 'USD',
                    unit: 'Route Server',
                    unitPrice: 2.50
                },
                {
                    currency: 'USD',
                    unit: 'Content Delivery Network',
                    unitPrice: 275.10
                }
            ],
            pricesAt: "".concat(new Date().getTime())
        },
        three: {
            monthlyEstimate: {
                cost: 26.28,
                currency: 'USD',
                description: 'Google Cloud Platform'
            },
            prices: [
                {
                    currency: 'USD',
                    unit: 'Transit Gateway',
                    unitPrice: 109.50
                }
            ],
            pricesAt: "".concat(new Date().getTime())
        }
    }
};
exports.NetspecConfig = config;
exports.Netspec = { config: config, status: status };
