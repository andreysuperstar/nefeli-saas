"use strict";
exports.__esModule = true;
exports.NetworksById = exports.Networks = void 0;
exports.Networks = {
    '1': {
        identifier: '1',
        name: 'prod-vpc',
        aws: {
            region: 'us-east-1'
        }
    },
    '2': {
        identifier: '2',
        name: 'dev-vpc',
        aws: {
            region: 'us-west-1'
        }
    },
    '3': {
        identifier: '3',
        name: 'qa-vnet',
        azure: {
            region: 'West Europe'
        }
    },
    '4': {
        identifier: '4',
        name: 'engr-vnet',
        azure: {
            region: 'West Europe'
        }
    }
};
var NetworksById = function (req, res) {
    res.status(200).jsonp(exports.Networks[req.params['id']]);
};
exports.NetworksById = NetworksById;
