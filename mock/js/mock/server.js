"use strict";
exports.__esModule = true;
var json_server_1 = require("json-server");
var routes_1 = require("./routes");
var PORT = 3000;
var server = (0, json_server_1.create)();
var middlewares = (0, json_server_1.defaults)();
routes_1.ROUTES.forEach(function (route) {
    if (typeof route.response === 'function') {
        server[route.method](route.path, route.response);
    }
    else {
        server[route.method](route.path, function (req, res) {
            res.status(route.status).jsonp(route.response);
        });
    }
});
server.use(middlewares);
server.listen(PORT, function () {
    console.log("JSON Server is running on port ".concat(PORT));
});
