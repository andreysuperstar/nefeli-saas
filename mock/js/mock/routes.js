"use strict";
exports.__esModule = true;
exports.ROUTES = void 0;
var netspecs_1 = require("./data/netspecs");
var networks_1 = require("./data/networks");
var METHODS;
(function (METHODS) {
    METHODS["GET"] = "get";
    METHODS["POST"] = "post";
})(METHODS || (METHODS = {}));
exports.ROUTES = [
    { method: METHODS.POST, path: '/v1/netspecs', status: 201, response: netspecs_1.NetspecConfig },
    { method: METHODS.POST, path: '/v1/netspecs/:id/approve', status: 201, response: {} },
    { method: METHODS.GET, path: '/v1/netspecs/:id', status: 200, response: netspecs_1.Netspec },
    { method: METHODS.GET, path: '/v1/networks', status: 200, response: networks_1.Networks },
    { method: METHODS.GET, path: '/v1/networks/:id', status: 200, response: networks_1.NetworksById }
];
