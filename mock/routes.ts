import { NetspecById, NetspecConfig } from './data/netspecs';
import { Networks, NetworksById } from './data/networks';
import { AwsProvider, FindProviders } from './data/providers';

enum METHODS {
  GET = 'get',
  POST = 'post',
  PUT = 'put'
}

interface Route {
  method: METHODS;
  path: string;
  status?: number;
  response?: any;
}

export const ROUTES: Route[] = [
  { method: METHODS.POST, path: '/v1/netspecs', status: 201, response: NetspecConfig },
  { method: METHODS.POST, path: '/v1/netspecs/:id/approve', status: 201, response: {} },
  { method: METHODS.GET, path: '/v1/netspecs/:id', status: 200, response: NetspecById },
  { method: METHODS.GET, path: '/v1/networks', status: 200, response: Networks },
  { method: METHODS.GET, path: '/v1/networks/:id', status: 200, response: NetworksById },
  { method: METHODS.GET, path: '/v1/providers', status: 200,  response: FindProviders },
  { method: METHODS.GET, path: '/v1/providers/:id', status: 200,  response: FindProviders },
  { method: METHODS.POST, path: '/v1/providers', status: 201, response: AwsProvider },
  { method: METHODS.PUT, path: '/v1/providers/:id/credentials', status: 204 },
  { method: METHODS.POST, path: '/v1/networks/:id/show', status: 201, response: {} },
  { method: METHODS.POST, path: '/v1/networks/:id/ignore', status: 201, response: {} },
  { method: METHODS.POST, path: '/v1/providers/1/discover', status: 201, response: {} }
];
