# Development Build Instructions
Nodejs 14.15.5 is required.

If you do not have it already, install [nvm](https://github.com/nvm-sh/nvm).  Dashboard still requires nodejs 10.20, so you probably want your environment to support multiple versions of nodejs.

Once you have nvm installed, run `nvm install 14.15.5`.  Switch to this version by doing `nvm use 14.15.5`.  Run `node -v` to confirm the proper version is now active. Finally, do `npm install` to install the project's node dependencies. Very IMPORTANT to make sure you're using the proper version of node when working on this project or the Tapestry Dashboard project.

You may now execute all of the normal npm commands for Angular projects.
- `npm run build` - create production build
- `npm run test` - execute all unit tests
- `npm start` - starts the dev server, application hosted on  http://localhost:4200

# Build via Make
For the time being, this project is not buildable via make, therefore not part of the Jenkins builds as of yet.