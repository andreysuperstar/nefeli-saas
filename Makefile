include ../common.mk

TARGET := saas_ui
DOCKER_CONTEXT := dist/ui

SHELL := /bin/bash
npm := source $(NVM_DIR)/nvm.sh && nvm exec $(SILENT_FLAG) 14.15.5 npm

.PHONY: build lint test clean distclean

build: code-gen
	$(MAKE) .build_timestamp

.build_timestamp: node_modules/.install_timestamp tsconfig.json angular.json $(shell find src) $(shell find mock)
	$(npm) run build-no-progress $(SILENCE_DEV_NULL)
	touch $@

lint: build
	$(npm) run $(SILENT_FLAG) lint

test: build
	$(npm) run $(SILENT_FLAG) test

clean:
	@rm -rf .build_timestamp dist rest_client mock/js coverage
	$(MAKE) -C ../ clean-rest

distclean: clean
	@rm -rf .angular node_modules
