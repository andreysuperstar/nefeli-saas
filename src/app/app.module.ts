import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BASE_PATH } from '../../rest_client/saas/variables';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { RequestUrlInterceptor } from './interceptors/request-url.interceptor';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FlexLayoutModule,
    AppRoutingModule
  ],
  providers: [
    {
      provide: BASE_PATH,
      useValue: '/v1'
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: RequestUrlInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
