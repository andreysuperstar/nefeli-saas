import { By } from '@angular/platform-browser';
import { ComponentFixture, inject, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatCardHarness } from '@angular/material/card/testing';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatButtonHarness } from '@angular/material/button/testing';
import { HarnessLoader } from '@angular/cdk/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';

import { ConnectResourcesComponent } from './connect-resources.component';

describe('ConnectResourcesComponent', () => {
  let component: ConnectResourcesComponent;
  let fixture: ComponentFixture<ConnectResourcesComponent>;
  let loader: HarnessLoader;

  beforeEach(async () => {
    await TestBed
      .configureTestingModule({
        imports: [
          RouterTestingModule,
          ReactiveFormsModule,
          MatCardModule,
          MatIconModule,
          MatButtonModule,
        ],
        declarations: [ConnectResourcesComponent]
      })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConnectResourcesComponent);
    component = fixture.componentInstance;
    loader = TestbedHarnessEnvironment.loader(fixture);

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render welcome section', () => {
    const welcomeSectionDe = fixture.debugElement.query(By.css('section.welcome'));

    expect(welcomeSectionDe)
      .withContext('render welcome section')
      .toBeDefined();

    const welcomeHeadingDe = welcomeSectionDe.query(By.css('h1'));


    expect(welcomeHeadingDe.nativeElement.textContent)
      .withContext('render welcome heading')
      .toBe('Welcome!');

    const welcomeMessageParagraphDe = welcomeSectionDe.query(By.css('p'));

    expect(welcomeMessageParagraphDe.nativeElement.textContent)
      .withContext('render welcome message')
      .toBe('Nefeli helps you create a global network connecting your cloud resources from different regions and providers, your data centers and offices, and even employee devices.');

    const saasArchImageDe = welcomeSectionDe.query(By.css('img'));

    expect(saasArchImageDe)
      .withContext('render SaaS Arch image')
      .toBeDefined();

    expect(saasArchImageDe.nativeElement.src)
      .withContext('render SaaS Arch image source')
      .toMatch('assets/saas-arch.png');

    expect(saasArchImageDe.nativeElement.alt)
      .withContext('render SaaS Arch image alt')
      .toBe('SaaS Arch');
  });

  it('should render resource card', async () => {
    const resourceCard = await loader.getHarness(MatCardHarness);

    await expectAsync(resourceCard.getTitleText())
      .withContext('render title text')
      .toBeResolvedTo('What Types of Resources Do You Want To Connect to Your Global Network?');

    await expectAsync(resourceCard.getSubtitleText())
      .withContext('render subtitle text')
      .toBeResolvedTo('You can always add more later.');

    const resourceLabels = fixture.debugElement.queryAll(By.css('label'));
    const resourceInputs = fixture.debugElement.queryAll(By.css('input[type="checkbox"]'));

    expect(resourceLabels.length)
      .withContext('render resource control labels')
      .toBe(3);

    expect(resourceInputs.length)
      .withContext('render resource controls')
      .toBe(3);

    await loader.getHarness(MatButtonHarness.with({
      text: 'Let\'s Get Started!'
    }));
  });

  it('should submit resource form', inject([Router], (router: Router) => async () => {
    const resourceLabel = fixture.debugElement.query(By.css('label'));

    resourceLabel.nativeElement.click();

    const startButton = await loader.getHarness(MatButtonHarness.with({
      text: 'Let\'s Get Started!'
    }));

    const navigateSpy = spyOn(router, 'navigate');

    await startButton.click();

    expect(navigateSpy)
      .withContext('navigate to Import resources page')
      .toHaveBeenCalledWith(['import-resources']);
  }));
});
