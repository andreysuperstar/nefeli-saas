import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'saas-connect-resources',
  templateUrl: './connect-resources.component.html',
  styleUrls: ['./connect-resources.component.less']
})
export class ConnectResourcesComponent {
  private _resourceForm = this._fb.group({
    cloud: undefined,
    enterprise: {
      value: undefined,
      disabled: true
    },
    nefeliEdge: {
      value: undefined,
      disabled: true
    }
  });

  constructor(private _router: Router, private _fb: FormBuilder) { }

  public submitResourceForm() {
    if (this._resourceForm.get('cloud')?.value === true) {
      void this._router.navigate(['import-resources']);
    }
  }

  public get resourceForm(): FormGroup {
    return this._resourceForm;
  }
}
