import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { FlexLayoutModule } from '@angular/flex-layout';

import { HomeRoutingModule } from './home-routing.module';

import { SharedModule } from '../shared/shared.module';

import { HomeComponent } from './home.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { ConnectResourcesComponent } from './connect-resources/connect-resources.component';

@NgModule({
  declarations: [
    HomeComponent,
    ToolbarComponent,
    ConnectResourcesComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatCardModule,
    MatIconModule,
    MatButtonModule,
    FlexLayoutModule,
    HomeRoutingModule,
    SharedModule
  ]
})
export class HomeModule { }
