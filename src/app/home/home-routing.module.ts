import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './home.component';
import { ConnectResourcesComponent } from './connect-resources/connect-resources.component';

export const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    children: [
      {
        path: '',
        component: ConnectResourcesComponent
      },
      {
        path: 'import-resources',
        loadChildren: () => import('../import-resources/import-resources.module')
          .then(m => m.ImportResourcesModule)
      },
      {
        path: 'create-netspec',
        loadChildren: () => import('../netspec-designer/netspec-designer.module').then(m => m.NetspecDesignerModule),
      },
      {
        path: 'netspec/:netspecId',
        loadChildren: () => import('../network/network.module')
          .then(m => m.NetworkModule),
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
