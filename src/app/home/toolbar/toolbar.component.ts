import { Component, OnInit } from '@angular/core';
import { FALLBACK, GravatarService } from '../../shared/gravatar.service';
import { AVATAR_MODE } from '../../shared/avatar/avatar.component';

export const FAKE_EMAIL = 'saas@nefeli.io';

@Component({
  selector: 'saas-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.less']
})
export class ToolbarComponent implements OnInit {

  private _gravatarUrl: string | undefined;

  constructor(
    private _gravatarService: GravatarService
  ) { }

  public ngOnInit(): void {
    // TODO:  FAKE_EMAIL should be replaced with real user email
    this._gravatarService
      .getGravatarUrl(FAKE_EMAIL, FALLBACK.MP)
      .subscribe(
        (url) => {
          this._gravatarUrl = url;
        }
      );
  }

  public get gravatarUrl(): string | undefined {
    return this._gravatarUrl;
  }

  public get AVATAR_MODE(): typeof AVATAR_MODE {
    return AVATAR_MODE;
  }
}
