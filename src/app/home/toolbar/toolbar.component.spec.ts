import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FAKE_EMAIL, ToolbarComponent } from './toolbar.component';
import { FALLBACK, GravatarService } from '../../shared/gravatar.service';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AvatarComponent } from '../../shared/avatar/avatar.component';
import { of } from 'rxjs';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

describe('ToolbarComponent', () => {
  let component: ToolbarComponent;
  let fixture: ComponentFixture<ToolbarComponent>;
  let gravatarService: GravatarService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        NoopAnimationsModule,
        HttpClientTestingModule,
      ],
      declarations: [
        AvatarComponent,
        ToolbarComponent
      ]
    })
      .compileComponents();
    gravatarService = TestBed.inject(GravatarService);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ToolbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    const logoDe = fixture.debugElement.query(By.css('div.logo > img'));
    expect(logoDe).not.toBeNull();
    expect(logoDe.nativeElement.src).toContain('assets/nefeli-logo.svg');

    const menuDe: DebugElement = fixture.debugElement.query(By.css('nav.menu'));
    expect(menuDe).not.toBeNull();
    const menuBtnDes: DebugElement[] = menuDe.queryAll(By.css('button'));
    expect(menuBtnDes.length).toEqual(3);
    expect(menuBtnDes[0].nativeElement.textContent).toContain('Overview');
    expect(menuBtnDes[1].nativeElement.textContent).toContain('Stats');
    expect(menuBtnDes[2].nativeElement.textContent).toContain('Diagnostics');

    const secondaryMenuDe: DebugElement = fixture.debugElement.query(By.css('div.secondary-menu'));
    expect(secondaryMenuDe).not.toBeNull();
    expect(secondaryMenuDe.query(By.css('saas-avatar'))).not.toBeNull();

    const settingBtnImgDe = secondaryMenuDe.query(By.css('button > img'));
    expect(settingBtnImgDe).not.toBeNull();
    expect(settingBtnImgDe.nativeElement.src).toContain('assets/icon-settings.svg')
  });

  it('should fetch gravatar url on init', () => {
    const emailHash = '593be52a46f869eea8b31d146d21de7a';
    const gravatarUrl = `https://www.gravatar.com/avatar/${emailHash}?d=${FALLBACK.MP}`;
    spyOn(gravatarService, 'getGravatarUrl').and.returnValue(of(gravatarUrl));
    component.ngOnInit();
    expect(component.gravatarUrl).toEqual(gravatarUrl);
    expect(gravatarService.getGravatarUrl).toHaveBeenCalledWith(FAKE_EMAIL, FALLBACK.MP);
  });
});
