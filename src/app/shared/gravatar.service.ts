import { Md5 } from 'ts-md5/dist/md5';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, mapTo, Observable, of } from 'rxjs';

export enum FALLBACK {
  MP = 'mp',
  NOT_FOUND = '404'
}

@Injectable({
  providedIn: 'root'
})
export class GravatarService {

  constructor(
    private _httpClient: HttpClient
  ) {
  }

  public getGravatarUrl(email: string, fallBack: FALLBACK = FALLBACK.NOT_FOUND): Observable<string | undefined> {
    const emailHash = Md5.hashStr(email.trim().toLowerCase());
    const url = `https://www.gravatar.com/avatar/${emailHash}?d=${fallBack}`;

    return this._httpClient.get(
      url,
      { responseType: 'text' }
    )
      .pipe(
        mapTo(url),
        catchError(() => of(undefined))
      );
  }
}
