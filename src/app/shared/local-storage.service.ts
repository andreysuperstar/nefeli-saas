import { Injectable } from '@angular/core';

export enum LocalStorageKey {
  LOG_FILE_SIZE = 'logFileSize',
  NETSPEC_NETWORK = 'netspecNetwork',
  NETSPEC_ROUTER = 'netspecRouter'
}

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {
  private _store = window.localStorage;

  public write(key: LocalStorageKey | string, data: string) {
    this._store.setItem(key, data);
  }

  public read(key: LocalStorageKey | string): string | null {
    return this._store.getItem(key);
  }

  public delete(key: LocalStorageKey) {
    this._store.removeItem(key);
  }
}
