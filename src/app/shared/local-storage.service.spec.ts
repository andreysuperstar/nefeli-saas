import { inject, TestBed } from '@angular/core/testing';

import { LocalStorageKey, LocalStorageService } from './local-storage.service';

describe('LocalStorageService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LocalStorageService]
    });
  });

  it('should be created', inject([LocalStorageService], (service: LocalStorageService) => {
    expect(service).toBeTruthy();
  }));

  it('should write to local storage', inject([LocalStorageService], (service: LocalStorageService) => {
    const testData = '10';
    service.write(LocalStorageKey.LOG_FILE_SIZE, testData);
    const storedData = service.read(LocalStorageKey.LOG_FILE_SIZE);

    expect(storedData).toBe(testData);
  }));

  it('should delete from local storage', inject([LocalStorageService], (service: LocalStorageService) => {
    const testData = '10';
    service.write(LocalStorageKey.LOG_FILE_SIZE, testData);
    service.delete(LocalStorageKey.LOG_FILE_SIZE);
    const storedData = service.read(LocalStorageKey.LOG_FILE_SIZE);

    expect(storedData).toBeFalsy();
  }));

});
