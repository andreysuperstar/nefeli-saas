import { TestBed } from '@angular/core/testing';
import { Md5 } from 'ts-md5/dist/md5';

import { FALLBACK, GravatarService } from './gravatar.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

describe('GravatarService', () => {
  let httpTestingController: HttpTestingController;
  let service: GravatarService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    httpTestingController = TestBed.inject(HttpTestingController);
    service = TestBed.inject(GravatarService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should generate gravatar url', (done) => {
    const emailHash = '1aedb8d9dc4751e229a335e371db8058';
    spyOn(Md5, 'hashStr').and.returnValue(emailHash as any);

    const email = 'Test@GMAIL.com';
    const url = `https://www.gravatar.com/avatar/${emailHash}?d=${FALLBACK.MP}`;

    service.getGravatarUrl(email, FALLBACK.MP).subscribe(res => {
      expect(res).toEqual(url);
      expect(Md5.hashStr).toHaveBeenCalledTimes(1);
      done();
    });
    const req = httpTestingController.expectOne(url);
    req.flush({});
  });

  it('should generate undefined gravatar url', (done) => {
    const emailHash = '1aedb8d9dc4751e229a335e371db8058';
    spyOn(Md5, 'hashStr').and.returnValue(emailHash as any);

    const email = 'Test@GMAIL.com';
    const url = `https://www.gravatar.com/avatar/${emailHash}?d=${FALLBACK.NOT_FOUND}`;

    service.getGravatarUrl(email).subscribe(res => {
      expect(res).toBeUndefined();
      expect(Md5.hashStr).toHaveBeenCalledTimes(1);
      done();
    });
    const req = httpTestingController.expectOne(url);
    req.flush({}, { status: 404, statusText: 'Not Found' });
  });
});
