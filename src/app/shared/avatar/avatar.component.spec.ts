import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AVATAR_MODE, AvatarComponent } from './avatar.component';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

describe('AvatarComponent', () => {
  let component: AvatarComponent;
  let fixture: ComponentFixture<AvatarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AvatarComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AvatarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render an image when url is specified', () => {
    const size = 30;
    const url = 'https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50?d=md';
    const name = 'user';
    component.size = size;
    component.mode = AVATAR_MODE.USER;
    component.url = url;
    component.name = name;
    fixture.detectChanges();

    let imgDe: DebugElement = fixture.debugElement.query(By.css('img'));
    expect(imgDe).not.toBeNull();
    let imgEl = imgDe.nativeElement as HTMLImageElement;
    expect(imgEl.classList).toContain('user-avatar');
    expect(imgEl.width).toEqual(size);
    expect(imgEl.height).toEqual(size);
    expect(imgEl.src).toEqual(url);
    expect(imgEl.alt).toEqual(name.toUpperCase());

    component.mode = AVATAR_MODE.DEFAULT;
    fixture.detectChanges();

    imgDe = fixture.debugElement.query(By.css('img'));
    expect(imgDe).not.toBeNull();
    imgEl = imgDe.nativeElement as HTMLImageElement;
    expect(imgEl.classList).not.toContain('user-avatar');
  });

  it('should render an div when url is unspecified', () => {
    const size = 30;
    const name = 'user';
    component.size = size;
    component.mode = AVATAR_MODE.USER;
    component.url = undefined;
    component.name = name;
    fixture.detectChanges();

    expect(fixture.debugElement.query(By.css('img'))).toBeNull();
    const divDe: DebugElement = fixture.debugElement.query(By.css('div.cap-avatar'));
    const divEl = divDe.nativeElement as HTMLImageElement;
    expect(divEl.classList).toContain('user-avatar');
    expect(divEl.style.width).toEqual(`${size}px`);
    expect(divEl.style.height).toEqual(`${size}px`);
    expect(divEl.querySelector('span.cap-letter')?.textContent).toContain(name[0].toUpperCase());
  });
});
