import { Component, Input } from '@angular/core';

const CAP_COLORS = ['blue', 'red', 'green'];
const DEFAULT_AVATAR_SIZE = 32;

export enum AVATAR_MODE {
  USER = 'user',
  DEFAULT = 'default'
}

@Component({
  selector: 'saas-avatar',
  templateUrl: './avatar.component.html',
  styleUrls: ['./avatar.component.less']
})
export class AvatarComponent {
  private _url: string | undefined;
  private _name = '';
  private _userName = '';
  private _firstName = '';
  private _lastName = '';
  private _size = DEFAULT_AVATAR_SIZE;
  private _mode: AVATAR_MODE = AVATAR_MODE.DEFAULT;

  @Input()
  public set url(value: string | undefined) {
    this._url = value;
  }

  @Input()
  public set name(value: string) {
    this._name = value?.toUpperCase();
  }

  @Input()
  public set mode(value: AVATAR_MODE) {
    this._mode = value;
  }

  @Input()
  public set userName(value: string) {
    this._userName = value?.toUpperCase();
  }

  @Input()
  public set firstName(value: string) {
    this._firstName = value?.toUpperCase();
  }

  @Input()
  public set lastName(value: string) {
    this._lastName = value?.toUpperCase();
  }

  @Input()
  public set size(value: number) {
    this._size = value;
  }

  public get url(): string | undefined {
    return this._url;
  }

  public get name(): string {
    return this._name;
  }

  public get userName(): string {
    return this._userName;
  }

  public get caption(): string {
    return this._mode === AVATAR_MODE.USER ? this.getInitials() : this._name[0];
  }

  public get size(): number {
    return this._size;
  }

  public get mode(): AVATAR_MODE {
    return this._mode;
  }

  public getCapColor(): string {
    const index = this.getColorIndex(this.name);

    return CAP_COLORS[index];
  }

  private getInitials(): string {
    let initials = this.name[0] ?? '';

    const hasFirstOrLastName = this._firstName || this._lastName;
    const hasFirstAndLastName = this._firstName && this._lastName;
    if (hasFirstAndLastName) {
      initials = this._firstName[0] + this._lastName[0];
    } else if (hasFirstOrLastName) {
      initials = hasFirstOrLastName[0];
    } else if (this._userName) {
      initials = this._userName[0];
    }

    return initials;
  }

  private getColorIndex(name: string): number {
    let sum = 0;
    let charCode: number;

    for (let i = 0, len = name?.length ?? 0; i < len; i++) {
      charCode = name[i].charCodeAt(0);
      sum += i > 0 ? charCode * i + i : charCode;
    }

    return sum % CAP_COLORS.length;
  }

  public get AVATAR_MODE(): typeof AVATAR_MODE {
    return AVATAR_MODE;
  }
}
