import { By } from '@angular/platform-browser';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { HarnessLoader, parallel } from '@angular/cdk/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { MatCardModule } from '@angular/material/card';
import { MatStepper, MatStepperModule } from '@angular/material/stepper';
import { MatStepperHarness } from '@angular/material/stepper/testing';
import { MatError, MatFormFieldModule } from '@angular/material/form-field';
import { MatFormFieldHarness } from '@angular/material/form-field/testing';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatButtonHarness } from '@angular/material/button/testing';
import { MatTableModule } from '@angular/material/table';
import { MatHeaderCellHarness, MatRowHarness, MatTableHarness } from '@angular/material/table/testing';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatCheckboxHarness } from '@angular/material/checkbox/testing';
import { MatChipsModule } from '@angular/material/chips';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatProgressSpinnerHarness } from '@angular/material/progress-spinner/testing';

import { ImportResourcesComponent } from './import-resources.component';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { MatInputHarness } from '@angular/material/input/testing';
import { Provider } from '../../../rest_client/saas/model/provider';
import { MatIconModule } from '@angular/material/icon';
import { MatSelectModule } from '@angular/material/select';
import { MatDividerModule } from '@angular/material/divider';
import { MatSelectHarness } from '@angular/material/select/testing';
import { Providers, AwsProvider, AzureProvider } from '../../../mock/data/providers';


import { ESpecNetworkUsageState } from '../../../rest_client/saas/model/eSpecNetworkUsageState';
import { ESpecNetworks } from '../../../rest_client/saas/model/eSpecNetworks';

import { ProvidersService } from '../../../rest_client/saas/api/providers.service';
import { ImportResourceService } from './import-resource.service';

import { resources } from '../../../mock/data/resources';

async function selectProvider(
  providerType: 'azure' | 'aws',
  providerName: string,
  loader: HarnessLoader,
  httpTestingController: HttpTestingController) {
  const providerSelect = await loader.getHarness<MatSelectHarness>(MatSelectHarness.with({
    selector: '[formControlName=provider]'
  }));
  const req = httpTestingController.expectOne('http://localhost/v1/providers?sort=asc(name)');
  const mockProviders: Provider[] = Providers.providers?.filter(p => p[providerType]) ?? [];
  req.flush({ providers: mockProviders });
  expect(req.request.method).toBe('GET');

  await providerSelect.open();
  await providerSelect.clickOptions({
    text: providerName
  });
}

async function fillAwsForm(
  loader: HarnessLoader,
  accountName: string,
  accessKey: string,
  secretKey: string,
  tag?: string) {

  const accountField = await loader.getHarness(MatFormFieldHarness.with({
    floatingLabelText: 'Account Name *'
  }));
  const accountNameControl = await accountField.getControl() as MatInputHarness;
  await accountNameControl.setValue(accountName);

  const accessKeyField = await loader.getHarness(MatFormFieldHarness.with({
    floatingLabelText: 'Access Key ID *'
  }));
  const accessKeyControl = await accessKeyField.getControl() as MatInputHarness;
  await accessKeyControl.setValue(accessKey);

  const secretKeyField = await loader.getHarness(MatFormFieldHarness.with({
    floatingLabelText: 'Secret Access Key *'
  }));
  const secretKeyControl = await secretKeyField.getControl() as MatInputHarness;
  await secretKeyControl.setValue(secretKey);

  if (tag) {
    const tagField = await loader.getHarness(MatFormFieldHarness.with({
      floatingLabelText: 'Tag'
    }));
    const tagControl = await tagField.getControl() as MatInputHarness;
    await tagControl.setValue(tag);
  }
}


async function fillAzureForm(
  loader: HarnessLoader,
  subscription: string,
  accessKey: string,
  secretKey: string,
  tenant?: string) {

  const subscriptionField = await loader.getHarness(MatFormFieldHarness.with({
    floatingLabelText: 'Subscription ID *'
  }));
  const subscriptionControl = await subscriptionField.getControl() as MatInputHarness;
  await subscriptionControl.setValue(subscription);

  const accessKeyField = await loader.getHarness(MatFormFieldHarness.with({
    floatingLabelText: 'Client ID *'
  }));
  const accessKeyControl = await accessKeyField.getControl() as MatInputHarness;
  await accessKeyControl.setValue(accessKey);

  const secretKeyField = await loader.getHarness(MatFormFieldHarness.with({
    floatingLabelText: 'Client Secret *'
  }));
  const secretKeyControl = await secretKeyField.getControl() as MatInputHarness;
  await secretKeyControl.setValue(secretKey);

  if (tenant) {
    const tagField = await loader.getHarness(MatFormFieldHarness.with({
      floatingLabelText: 'Tenant ID'
    }));
    const tagControl = await tagField.getControl() as MatInputHarness;
    await tagControl.setValue(tenant);
  }
}

async function clickDiscoveryBtn(loader: HarnessLoader) {
  const discoveryButton = await loader.getHarness(MatButtonHarness.with({
    text: 'Start Discovering…'
  }));
  await discoveryButton.click();
}

describe('ImportResourcesComponent', () => {
  let component: ImportResourcesComponent;
  let fixture: ComponentFixture<ImportResourcesComponent>;
  let loader: HarnessLoader;
  let httpTestingController: HttpTestingController;

  beforeEach(async () => {
    await TestBed
      .configureTestingModule({
        imports: [
          NoopAnimationsModule,
          RouterTestingModule,
          ReactiveFormsModule,
          MatCardModule,
          MatStepperModule,
          MatFormFieldModule,
          MatInputModule,
          MatButtonModule,
          MatTableModule,
          MatCheckboxModule,
          MatChipsModule,
          MatProgressSpinnerModule,
          HttpClientTestingModule,
          MatIconModule,
          MatSelectModule,
          MatDividerModule
        ],
        declarations: [ImportResourcesComponent],
        providers: [
          ProvidersService,
          ImportResourceService
        ]
      })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ImportResourcesComponent);
    component = fixture.componentInstance;
    loader = TestbedHarnessEnvironment.loader(fixture);
    httpTestingController = TestBed.inject(HttpTestingController);

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render select platform step', async () => {
    const stepper = await loader.getHarness(MatStepperHarness);
    const [step] = await stepper.getSteps();

    await expectAsync(step.getLabel())
      .withContext('render step label')
      .toBeResolvedTo('Select platform');

    await expectAsync(step.isOptional())
      .withContext('render step required')
      .toBeResolvedTo(false);

    const stepDe = fixture.debugElement.query(By.css('.mat-horizontal-stepper-content'));
    const stepHeadingDe = stepDe.query(By.css('h1'));

    expect(stepHeadingDe.nativeElement.textContent)
      .withContext('render step heading')
      .toBe('Discover Resources from Which Platform?');

    const [awsButtonImageDe, azureButtonImageDe] = stepDe.queryAll(By.css('button img'));

    expect(awsButtonImageDe)
      .withContext('render AWS button image')
      .toBeDefined();

    expect(awsButtonImageDe.nativeElement.src)
      .withContext('render AWS button image source')
      .toMatch('assets/aws.png');

    expect(awsButtonImageDe.nativeElement.alt)
      .withContext('render AWS button image alt')
      .toBe('AWS');

    expect(azureButtonImageDe)
      .withContext('render Azure button image')
      .toBeDefined();

    expect(azureButtonImageDe.nativeElement.src)
      .withContext('render Azure button image source')
      .toMatch('assets/azure.svg');

    expect(azureButtonImageDe.nativeElement.alt)
      .withContext('render Azure button image alt')
      .toBe('Azure');
  });

  it('should render AWS credentials form',
    async () => {
      const awsButtonButtonDe = fixture.debugElement.query(By.css('.mat-horizontal-stepper-content button'));
      awsButtonButtonDe.nativeElement.click();

      const stepper = await loader.getHarness(MatStepperHarness);
      const [, step] = await stepper.getSteps();

      await expectAsync(step.isSelected())
        .withContext('step is selected')
        .toBeResolvedTo(true);
      await expectAsync(step.getLabel())
        .withContext('render step label')
        .toBeResolvedTo('Provide credentials');

      const providerSelect = await loader.getHarness<MatSelectHarness>(MatSelectHarness.with({
        selector: '[formControlName=provider]'
      }));

      const req = httpTestingController.expectOne('http://localhost/v1/providers?sort=asc(name)');
      const mockProviders: Provider[] = Providers.providers?.filter(p => p.aws) ?? [];
      req.flush({ providers: mockProviders });
      expect(req.request.method).toBe('GET');

      await providerSelect.open();
      const providerOptions = await providerSelect.getOptions();
      const providerOptionTexts = await Promise.all(providerOptions.map(option => option.getText()));
      expect(providerOptionTexts).toEqual([...mockProviders.map(p => p.name ?? ''), 'Create new...']);
      await expectAsync(providerSelect.getValueText()).toBeResolvedTo('Create new...');

      const formFields = await loader.getAllHarnesses(MatFormFieldHarness);
      await expectAsync(parallel(() => formFields.map(field => field.getLabel())))
        .withContext('render form field labels')
        .toBeResolvedTo(['Select provider...', 'Account Name *', 'Access Key ID *', 'Secret Access Key *', 'Tag']);

      const discoveryButton = await loader.getHarness(MatButtonHarness.with({
        text: 'Start Discovering…'
      }));

      expect(await discoveryButton.isDisabled()).toBeTruthy();
    });

  it('should render Azure credentials form', async () => {
    const [, azureButtonButtonDe] = fixture.debugElement.queryAll(By.css('.mat-horizontal-stepper-content button'));

    azureButtonButtonDe.nativeElement.click();

    const stepper = await loader.getHarness(MatStepperHarness);
    const [, step] = await stepper.getSteps();

    await expectAsync(step.isSelected())
      .withContext('step is selected')
      .toBeResolvedTo(true);

    await expectAsync(step.getLabel())
      .withContext('render step label')
      .toBeResolvedTo('Provide credentials');

    await expectAsync(step.isOptional())
      .withContext('render step required')
      .toBeResolvedTo(false);

    const [, stepDe] = fixture.debugElement.queryAll(By.css('.mat-horizontal-stepper-content'));
    const stepHeadingDe = stepDe.query(By.css('h1'));

    expect(stepHeadingDe.nativeElement.textContent)
      .withContext('render step heading')
      .toBe('Enter Azure Credentials');

    const providerSelect = await loader.getHarness<MatSelectHarness>(MatSelectHarness.with({
      selector: '[formControlName=provider]'
    }));

    const req = httpTestingController.expectOne('http://localhost/v1/providers?sort=asc(name)');
    const mockProviders: Provider[] = Providers.providers?.filter(p => p.azure) ?? [];
    req.flush({ providers: mockProviders });
    expect(req.request.method).toBe('GET');

    await providerSelect.open();
    const providerOptions = await providerSelect.getOptions();
    const providerOptionTexts = await Promise.all(providerOptions.map(option => option.getText()));
    expect(providerOptionTexts).toEqual([...mockProviders.map(p => p.name ?? ''), 'Create new...']);
    await expectAsync(providerSelect.getValueText()).toBeResolvedTo('Create new...');

    const formFields = await loader.getAllHarnesses(MatFormFieldHarness);
    await expectAsync(parallel(() => formFields.map(field => field.getLabel())))
      .withContext('render form field labels')
      .toBeResolvedTo(['Select provider...', 'Subscription ID *', 'Tenant ID', 'Client ID *', 'Client Secret *']);

    const discoveryButton = await loader.getHarness(MatButtonHarness.with({
      text: 'Start Discovering…'
    }));

    expect(await discoveryButton.isDisabled()).toBeTruthy();
  });

  it('should create AWS provider', fakeAsync(async () => {
    const accountName = 'name';
    const accessKey = 'access key';
    const secretKey = 'secret key';
    const tag = 'tag1';

    const [awsButtonButtonDe] = fixture.debugElement.queryAll(By.css('.mat-horizontal-stepper-content button'));
    awsButtonButtonDe.nativeElement.click();
    fixture.detectChanges();

    await selectProvider('aws', 'Create new...', loader, httpTestingController);
    fixture.detectChanges();

    await fillAwsForm(loader, accountName, accessKey, secretKey, tag);
    const discoveryButton = await loader.getHarness(MatButtonHarness.with({
      text: 'Start Discovering…'
    }));

    expect(await discoveryButton.isDisabled()).toBeFalsy();
    const nextSpy = spyOn(component['_stepper'] as MatStepper, 'next');

    await discoveryButton.click();
    let req = httpTestingController.expectOne('http://localhost/v1/providers');
    const identifier = 'TEST';
    const provider: Provider = {
      identifier,
      aws: {},
      name: accountName,
      tags: { [tag]: tag }
    };
    req.flush(provider);
    expect(req.request.method).toBe('POST');
    expect(req.request.body.name).toBe(accountName);
    expect(req.request.body.aws).toEqual({});
    expect(req.request.body.tags).toEqual({ [tag]: tag });

    req = httpTestingController.expectOne(`http://localhost/v1/providers/${identifier}/credentials`);
    req.flush({});
    expect(req.request.method).toBe('PUT');
    expect(req.request.body.aws).toEqual({
      accessKeyId: accessKey,
      secretAccessKey: secretKey
    });

    tick();
    expect(nextSpy).toHaveBeenCalled();
  }));

  it('should use existing AWS provider', async () => {
    const awsButtonButtonDe = fixture.debugElement.query(By.css('.mat-horizontal-stepper-content button'));
    awsButtonButtonDe.nativeElement.click();
    fixture.detectChanges();

    await selectProvider('aws', AwsProvider.name!, loader, httpTestingController);
    fixture.detectChanges();

    const discoveryButton = await loader.getHarness(MatButtonHarness.with({
      text: 'Start Discovering…'
    }));
    expect(await discoveryButton.isDisabled()).toBeFalsy();
    const nextSpy = spyOn(component['_stepper'] as MatStepper, 'next');

    await discoveryButton.click();

    await fixture.whenStable();
    expect(nextSpy).toHaveBeenCalled();
  });

  it('should show an error of AWS provider creation', async () => {
    const accountName = 'name';
    const accessKey = 'access key';
    const secretKey = 'secret key';

    const awsButtonButtonDe = fixture.debugElement.query(By.css('.mat-horizontal-stepper-content button'));
    awsButtonButtonDe.nativeElement.click();
    fixture.detectChanges();


    await selectProvider('aws', 'Create new...', loader, httpTestingController);
    fixture.detectChanges();

    await fillAwsForm(loader, accountName, accessKey, secretKey);
    await clickDiscoveryBtn(loader);
    const req = httpTestingController.expectOne('http://localhost/v1/providers');
    const mockErrorResponse = {
      status: 500,
      statusText: 'Error',
    };
    req.flush({}, mockErrorResponse);
    fixture.detectChanges();

    const errorDe = fixture.debugElement.query(By.directive(MatError));
    expect(errorDe.nativeElement.textContent).toContain('Can not create provider: ');
  });

  const goToAwsDiscoveryTable = async (): Promise<string> => {
    const awsButtonButtonDe = fixture.debugElement.query(By.css('.mat-horizontal-stepper-content button'));
    awsButtonButtonDe.nativeElement.click();
    fixture.detectChanges();

    await selectProvider('aws', 'Create new...', loader, httpTestingController);
    fixture.detectChanges();


    const accountName = 'name';
    const accessKey = 'access key';
    const secretKey = 'secret key';
    const tag = 'tag1';
    await fillAwsForm(loader, accountName, accessKey, secretKey, tag);

    const startDiscoveringButton = await loader.getHarness(MatButtonHarness.with({
      text: 'Start Discovering…'
    }));

    await startDiscoveringButton.click();
    const identifier = 'TEST';
    httpTestingController.expectOne('http://localhost/v1/providers').flush({ identifier });
    httpTestingController.expectOne(`http://localhost/v1/providers/${identifier}/credentials`).flush({});

    return identifier;
  }

  it('should render discovery and selection step', fakeAsync(async () => {
    const identifier = await goToAwsDiscoveryTable();
    fixture.detectChanges();

    // Discovery resources
    let stepper = await loader.getHarness(MatStepperHarness);
    let [, , step] = await stepper.getSteps();

    await expectAsync(step.isSelected())
      .withContext('dicovery and selection step is selected')
      .toBeResolvedTo(true);

    await expectAsync(step.getLabel())
      .withContext('render step label')
      .toBeResolvedTo('Discovery & Selection');

    await expectAsync(step.isOptional())
      .withContext('render step required')
      .toBeResolvedTo(false);

    await loader.getHarness(MatProgressSpinnerHarness);

    const [, , stepDe] = fixture.debugElement.queryAll(By.css('.mat-horizontal-stepper-content'));
    const stepDiscoveringParagraphDe = stepDe.query(By.css('p'));

    expect(stepDiscoveringParagraphDe.nativeElement.textContent)
      .withContext('render discovering paragraph')
      .toBe('Discovering resources…');

    let mockProviderResponse: Provider = {
      lastDiscoveryAttemptedAt: '1648573333325',
      lastDiscoverySucceededAt: '1648573333325'
    };

    httpTestingController
      .expectOne({
        method: 'GET',
        url: `http://localhost/v1/providers/${identifier}`
      })
      .flush(mockProviderResponse);

    httpTestingController
      .expectOne({
        method: 'POST',
        url: `http://localhost/v1/providers/${identifier}/discover`
      })
      .flush({});

    mockProviderResponse = {
      lastDiscoveryAttemptedAt: '1648573333330',
      lastDiscoverySucceededAt: '1648573333330'
    };

    httpTestingController
      .expectOne({
        method: 'GET',
        url: `http://localhost/v1/providers/${identifier}`
      })
      .flush(mockProviderResponse);

    const mockNetworks: ESpecNetworks = {
      networks: resources
    };

    httpTestingController
      .expectOne({
        method: 'GET',
        url: `http://localhost/v1/providers/${identifier}/networks?filter=eq(usageState${encodeURIComponent(',')}${ESpecNetworkUsageState.Discovered})`
      })
      .flush(mockNetworks);

    await loader.getHarness(MatTableHarness);

    const headerCells = await loader.getAllHarnesses(MatHeaderCellHarness);

    await expectAsync(parallel(() => headerCells.map(row => row.getText())))
      .withContext('render resource table header cells')
      .toBeResolvedTo(['', 'Name', 'Tags']);

    const rows = await loader.getAllHarnesses(MatRowHarness);
    const rowCellsText = await parallel(() => rows.map(row => row.getCellTextByIndex()));

    rowCellsText.forEach((row, index) => {
      const [, name, tags] = row;

      expect(name)
        .withContext(`render resource table ${name} row name cell`)
        .toBe(resources[index].name ?? '');

      const tagEntries = Object.entries(resources[index].tags ?? {});

      tagEntries.forEach(([key, value]) => {
        expect(tags)
          .withContext(`render resource table ${name} row tags cell`)
          .toContain(`${key} | ${value}`)
      });
    });

    const importMoreButton = await loader.getHarness(MatButtonHarness.with({
      text: 'Import More…'
    }));

    await importMoreButton.click();

    stepper = await loader.getHarness(MatStepperHarness);
    [step] = await stepper.getSteps();

    await expectAsync(step.isSelected())
      .withContext('select platform step is selected')
      .toBeResolvedTo(true);

    await loader.getHarness(MatButtonHarness.with({
      text: 'Done'
    }));
  }));

  it('should redirect from discovery to credentials step on error', fakeAsync(async () => {
    const identifier = await goToAwsDiscoveryTable();

    let stepper = await loader.getHarness(MatStepperHarness);
    let [, , step] = await stepper.getSteps();

    await expectAsync(step.isSelected())
      .withContext('dicovery and selection step is selected')
      .toBeResolvedTo(true);

    let mockProviderResponse: Provider = {
      lastDiscoveryAttemptedAt: '1648573333325',
      lastDiscoverySucceededAt: '1648573333325'
    };

    httpTestingController
      .expectOne({
        method: 'GET',
        url: `http://localhost/v1/providers/${identifier}`
      })
      .flush(mockProviderResponse);

    httpTestingController
      .expectOne({
        method: 'POST',
        url: `http://localhost/v1/providers/${identifier}/discover`
      })
      .flush({});

    mockProviderResponse = {
      lastDiscoveryAttemptedAt: '1648573333330',
      lastDiscoverySucceededAt: undefined,
      discoveryMessage: `Discovery failed: credentials for AWS provider ${identifier} are not AWS credentials`,
    };

    httpTestingController
      .expectOne({
        method: 'GET',
        url: `http://localhost/v1/providers/${identifier}`
      })
      .flush(mockProviderResponse);

    httpTestingController.expectNone({
      method: 'GET',
      url: `http://localhost/v1/providers/${identifier}/networks?filter=eq(usageState${encodeURIComponent(',')}${ESpecNetworkUsageState.Discovered})`
    });

    stepper = await loader.getHarness(MatStepperHarness);
    [, step] = await stepper.getSteps();

    await expectAsync(step.isSelected())
      .withContext('redirected to credentials step')
      .toBeResolvedTo(true);
  }));

  it('should create Azure provider', fakeAsync(async () => {
    const subscription = 'subscription';
    const accessKey = 'access key';
    const secretKey = 'secret key';
    const tenant = 'tenant';

    const [, azureButtonButtonDe] = fixture.debugElement.queryAll(By.css('.mat-horizontal-stepper-content button'));
    azureButtonButtonDe.nativeElement.click();
    fixture.detectChanges();

    await selectProvider('azure', 'Create new...', loader, httpTestingController);
    fixture.detectChanges();

    await fillAzureForm(loader, subscription, accessKey, secretKey, tenant);
    const discoveryButton = await loader.getHarness(MatButtonHarness.with({
      text: 'Start Discovering…'
    }));

    expect(await discoveryButton.isDisabled()).toBeFalsy();
    const nextSpy = spyOn(component['_stepper'] as MatStepper, 'next');

    await discoveryButton.click();
    let req = httpTestingController.expectOne('http://localhost/v1/providers');
    const identifier = 'TEST';
    const provider: Provider = {
      identifier,
      azure: {},
      name: subscription
    };
    req.flush(provider);
    expect(req.request.method).toBe('POST');
    expect(req.request.body.name).toBe(subscription);
    expect(req.request.body.azure).toEqual({});

    req = httpTestingController.expectOne(`http://localhost/v1/providers/${identifier}/credentials`);
    req.flush({});
    expect(req.request.method).toBe('PUT');
    expect(req.request.body.azure).toEqual({
      clientId: accessKey,
      clientSecret: secretKey,
      subscriptionId: subscription,
      tenantId: tenant
    });

    tick();
    expect(nextSpy).toHaveBeenCalled();
  }));

  it('should use existing Azure provider', async () => {
    const [, azureButtonButtonDe] = fixture.debugElement.queryAll(By.css('.mat-horizontal-stepper-content button'));
    azureButtonButtonDe.nativeElement.click();
    fixture.detectChanges();

    await selectProvider('azure', AzureProvider.name as string, loader, httpTestingController);
    fixture.detectChanges();

    const discoveryButton = await loader.getHarness(MatButtonHarness.with({
      text: 'Start Discovering…'
    }));
    expect(await discoveryButton.isDisabled()).toBeFalsy();
    const nextSpy = spyOn(component['_stepper'] as MatStepper, 'next');

    await discoveryButton.click();
    await fixture.whenStable();
    expect(nextSpy).toHaveBeenCalled();
  });

  it('should show an error of Azure provider creation', async () => {
    const subscription = 'subscription';
    const accessKey = 'access key';
    const secretKey = 'secret key';

    const [, azureButtonButtonDe] = fixture.debugElement.queryAll(By.css('.mat-horizontal-stepper-content button'));
    azureButtonButtonDe.nativeElement.click();
    fixture.detectChanges();

    await selectProvider('azure', 'Create new...', loader, httpTestingController);
    fixture.detectChanges();

    await fillAzureForm(loader, subscription, accessKey, secretKey);
    await clickDiscoveryBtn(loader);
    const req = httpTestingController.expectOne('http://localhost/v1/providers');
    const mockErrorResponse = {
      status: 500,
      statusText: 'Error',
    };
    req.flush({}, mockErrorResponse);
    fixture.detectChanges();

    const errorDe = fixture.debugElement.query(By.directive(MatError));
    expect(errorDe.nativeElement.textContent).toContain('Can not create provider: ');
  });

  it('should mark resources for import or ignore', async () => {
    spyOn(component['_router'], 'navigate');

    const identifier = await goToAwsDiscoveryTable();

    let mockProviderResponse: Provider = {
      lastDiscoveryAttemptedAt: '1648573333325',
      lastDiscoverySucceededAt: '1648573333325'
    };

    httpTestingController
      .expectOne({
        method: 'GET',
        url: `http://localhost/v1/providers/${identifier}`
      })
      .flush(mockProviderResponse);

    httpTestingController
      .expectOne({
        method: 'POST',
        url: `http://localhost/v1/providers/${identifier}/discover`
      })
      .flush({});

    mockProviderResponse = {
      lastDiscoveryAttemptedAt: '1648573333330',
      lastDiscoverySucceededAt: '1648573333330'
    };

    httpTestingController
      .expectOne({
        method: 'GET',
        url: `http://localhost/v1/providers/${identifier}`
      })
      .flush(mockProviderResponse);

    const mockNetworks: ESpecNetworks = {
      networks: resources
    };

    httpTestingController
      .expectOne({
        method: 'GET',
        url: `http://localhost/v1/providers/${identifier}/networks?filter=eq(usageState${encodeURIComponent(',')}${ESpecNetworkUsageState.Discovered})`
      })
      .flush(mockNetworks);

    fixture.detectChanges();

    const table = await loader.getHarness(MatTableHarness);
    const rows = await table.getRows()

    const clickRow = async (index: number) => {
      const cells = await rows[index].getCells();
      const checkBox = await cells[0].getHarness(MatCheckboxHarness);
      await checkBox.check();
    }

    await clickRow(0);
    await clickRow(2);

    const doneButton = await loader.getHarness(MatButtonHarness.with({
      text: 'Done'
    }));
    await doneButton.click();

    httpTestingController.expectOne(`http://localhost/v1/networks/${resources[0].identifier ?? ''}/show`).flush({});
    httpTestingController.expectOne(`http://localhost/v1/networks/${resources[1].identifier ?? ''}/ignore`).flush({});
    httpTestingController.expectOne(`http://localhost/v1/networks/${resources[2].identifier ?? ''}/show`).flush({});
    httpTestingController.expectOne(`http://localhost/v1/networks/${resources[3].identifier ?? ''}/ignore`).flush({});
    httpTestingController.expectOne(`http://localhost/v1/networks/${resources[4].identifier ?? ''}/ignore`).flush({});
    httpTestingController.expectOne(`http://localhost/v1/networks/${resources[5].identifier ?? ''}/ignore`).flush({});
    httpTestingController.expectOne(`http://localhost/v1/networks/${resources[6].identifier ?? ''}/ignore`).flush({});

    expect(component['_router'].navigate).toHaveBeenCalledWith(['create-netspec']);
  });
});
