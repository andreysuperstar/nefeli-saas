import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ImportResourcesComponent } from './import-resources.component';

const routes: Routes = [
  {
    path: '',
    component: ImportResourcesComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ImportResourcesRoutingModule { }
