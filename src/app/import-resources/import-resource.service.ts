import { Injectable } from '@angular/core';
import { Provider } from '../../../rest_client/saas/model/provider';
import { ProvidersService } from '../../../rest_client/saas/api/providers.service';
import { concat, Observable, tap, timer, of, throwError } from 'rxjs';
import { shareReplay, skip, repeat, mapTo, mergeMap, concatMap, find } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';


export interface DiscoveryStatus {
  success: boolean,
  discoveryMessage?: string
}

const Config = {
  PROVIDER_POLL_INTERVAL: 5000
}

@Injectable({
  providedIn: 'root'
})
export class ImportResourceService {
  private _lastDiscoveryAttemptedAt: string | undefined;
  private _lastDiscoverySucceededAt: string | undefined;

  constructor(
      private _providerService: ProvidersService
  ) { }

  public discoverResources(providerId: string): Observable<DiscoveryStatus | undefined> {

    return this._providerService.getProvider(providerId)
      .pipe(
        tap((provider: Provider) => {
          this._lastDiscoveryAttemptedAt = provider?.lastDiscoveryAttemptedAt;
          this._lastDiscoverySucceededAt = provider?.lastDiscoverySucceededAt;
        }),
        mergeMap(() => this._providerService.postProviderDiscovery(providerId)),
        concatMap(() => this.streamProvider(providerId)),
        find((provider: Provider | undefined ) => this._lastDiscoveryAttemptedAt !== provider?.lastDiscoveryAttemptedAt),
        mergeMap<Provider | undefined, Observable<DiscoveryStatus | undefined>>((provider: Provider | undefined) => {
          if (!provider?.lastDiscoverySucceededAt || provider?.lastDiscoverySucceededAt === this._lastDiscoverySucceededAt) {
            const error = new HttpErrorResponse({
              error: {
                message: provider?.discoveryMessage
              }
            });

            return throwError(() => error);
          }

          const discoveryStatus: DiscoveryStatus = {
            success: true,
            discoveryMessage: provider?.discoveryMessage
          };

          return of(discoveryStatus);
        })
      );
  }


  public streamProvider(providerId: string): Observable<Provider | undefined> {
    return concat(
      this._providerService.getProvider(providerId),
      timer(Config.PROVIDER_POLL_INTERVAL).pipe(
        mapTo(undefined),
        skip(1)
      )
    ).pipe(
      repeat(),
      shareReplay({
        refCount: true
      })
    );
  }
}
