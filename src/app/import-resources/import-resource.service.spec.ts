import { Provider } from '../../../rest_client/saas/model/provider';
import { TestBed } from '@angular/core/testing';
import { HttpErrorResponse } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { catchError, EMPTY } from 'rxjs';
import { ImportResourceService, DiscoveryStatus } from './import-resource.service';


const mockProviderResponse: Provider = {
  lastDiscoveryAttemptedAt: '1648573333325',
  lastDiscoverySucceededAt: '1648573333325'
}

describe('ImportResourceService',  () => {
  let http: HttpTestingController;
  let service: ImportResourceService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    }).compileComponents();

    http = TestBed.inject(HttpTestingController);
    service = TestBed.inject(ImportResourceService);
  });


  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should discover resources', () => {
    const providerId = 'a1b2c3';

    service.discoverResources(providerId).subscribe((status: DiscoveryStatus | undefined) => {
      expect(status?.success).toBeTruthy();
    });

    http.expectOne(`http://localhost/v1/providers/${providerId}`).flush(mockProviderResponse);

    http.expectOne({
      method: 'POST',
      url: `http://localhost/v1/providers/${providerId}/discover`
    }).flush({});

    http.expectOne(`http://localhost/v1/providers/${providerId}`).flush({
      lastDiscoveryAttemptedAt: '1648573333330',
      lastDiscoverySucceededAt: '1648573333330'
    })
  });

  it('should throw error on discover resources failure', () => {
    const providerId = 'a1b2c3';
    const discoveryMessage = `Discovery failed: credentials for AWS provider ${providerId} are not AWS credentials`;

    service
      .discoverResources(providerId)
      .pipe(
        catchError(({
          error: { message }
        }: HttpErrorResponse) => {
          expect(message)
            .withContext('throw error with discovery message')
            .toBe(discoveryMessage);

          return EMPTY;
        })
      )
      .subscribe();

    http.expectOne(`http://localhost/v1/providers/${providerId}`).flush(mockProviderResponse);

    http.expectOne({
      method: 'POST',
      url: `http://localhost/v1/providers/${providerId}/discover`
    }).flush({});

    http.expectOne(`http://localhost/v1/providers/${providerId}`).flush({
      lastDiscoveryAttemptedAt: '1648573333330',
      lastDiscoverySucceededAt: undefined,
      discoveryMessage
    })
  });
});
