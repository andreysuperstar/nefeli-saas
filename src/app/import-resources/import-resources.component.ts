import { Component, ViewChild } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { SelectionModel } from '@angular/cdk/collections';
import { MatStepper } from '@angular/material/stepper';
import { ESpecNetworkUsageState } from '../../../rest_client/saas/model/eSpecNetworkUsageState';
import { ESpecNetwork } from '../../../rest_client/saas/model/eSpecNetwork';
import { ESpecNetworks } from '../../../rest_client/saas/model/eSpecNetworks';
import { ProvidersService } from '../../../rest_client/saas/api/providers.service';
import { ImportResourceService } from './import-resource.service';
import { Provider } from '../../../rest_client/saas/model/provider';
import { catchError, EMPTY, finalize, mergeMap, Observable, Subscription, tap, map, forkJoin } from 'rxjs';
import { IFormBuilder, IFormGroup } from '@rxweb/types';
import { AWSCredentials } from '../../../rest_client/saas/model/aWSCredentials';
import { MatTableDataSource } from '@angular/material/table';
import { AzureCredentials } from '../../../rest_client/saas/model/azureCredentials';
import { Credentials } from '../../../rest_client/saas/model/credentials';
import { NetworksService } from '../../../rest_client/saas/api/networks.service';
import { Router } from '@angular/router';

enum CloudPlatform {
  AWS,
  Azure
}

const RESOURCE_TABLE_COLUMNS = ['select', 'name', 'tags'];

export interface AwsCredentialsData {
  name: string;
  accessKey: string;
  secretKey: string;
  tag?: string;
}

export interface AwsProviderForm {
  provider?: Provider;
  credentials?: AwsCredentialsData;
}

export interface AzureCredentialsData {
  subscription: string;
  accessKey: string;
  secretKey: string;
  tenant?: string;
}

export interface AzureCredentialsForm {
  provider?: Provider;
  credentials?: AzureCredentialsData;
}

@Component({
  selector: 'saas-import-resources',
  templateUrl: './import-resources.component.html',
  styleUrls: ['./import-resources.component.less']
})
export class ImportResourcesComponent {
  private _platform?: CloudPlatform;
  private _fb: IFormBuilder;
  private _awsCredentialsForm: IFormGroup<AwsProviderForm>;
  private _azureCredentialsForm: IFormGroup<AzureCredentialsForm>;
  private _submittingCredentialsForm = false;
  private _error?: string;
  private _discoverResourcesSubscription?: Subscription;
  private _provider?: Provider;
  private _dataSource = new MatTableDataSource<ESpecNetwork>(undefined);
  private _selection = new SelectionModel<ESpecNetwork>(true);
  private _providers$?: Observable<Provider[]>;

  @ViewChild('stepper', {
    static: true
  }) private _stepper?: MatStepper;

  constructor(
    fb: FormBuilder,
    private _providerService: ProvidersService,
    private _networkService: NetworksService,
    private _router: Router,
    private _importResourceService: ImportResourceService
  ) {
    this._fb = fb;
    this._awsCredentialsForm = this._fb.group<AwsProviderForm>({
      provider: undefined,
      credentials: this._fb.group<AwsCredentialsData>({
        name: ['', Validators.required],
        accessKey: ['', Validators.required],
        secretKey: ['', Validators.required],
        tag: undefined
      })
    });
    this._azureCredentialsForm = this._fb.group<AzureCredentialsForm>({
      provider: undefined,
      credentials: this._fb.group<AzureCredentialsData>({
        subscription: ['', Validators.required],
        accessKey: ['', Validators.required],
        secretKey: ['', Validators.required],
        tenant: undefined
      })
    });
  }

  public onSelectPlatform(platform: CloudPlatform) {
    this._platform = platform;
    this._provider = undefined;

    this._providers$ = this._providerService.getProviders(undefined, undefined, undefined, ['asc(name)']).pipe(
      map(response => response.providers?.filter((provider: Provider) =>
        platform === CloudPlatform.AWS && provider.aws
        || platform === CloudPlatform.Azure && provider.azure
      ) ?? [])
    );
    setTimeout(() => this._stepper?.next());
  }

  private setDataSourceData(networks?: ESpecNetwork[]) {
    if (networks) {
      this._dataSource.data = networks;
    }
  }

  private discoverResources(provider: Provider): Observable<ESpecNetworks> {
    this._provider = provider;

    setTimeout(() => this._stepper?.next());

    return this._importResourceService
      .discoverResources(this._provider.identifier as string)
      .pipe(
        mergeMap(() => this._providerService.getProviderNetworks(provider.identifier as string, undefined, undefined, [`eq(usageState,${ESpecNetworkUsageState.Discovered})`])),
        catchError(() => {
          const credentialsStep = this._stepper?.steps.get(1);

          if (credentialsStep) {
            credentialsStep.editable = true;
          }

          this._stepper?.previous();

          if (credentialsStep) {
            credentialsStep.editable = false;
          }

          this._provider = undefined;

          return EMPTY;
        }),
        tap(({ networks }) => this.setDataSourceData(networks))
      );
  }

  public submitAwsCredentialsForm() {
    const { value, invalid } = this._awsCredentialsForm;
    const selectedProvider = value?.provider;
    if (selectedProvider) {
      this._discoverResourcesSubscription?.unsubscribe();
      this._discoverResourcesSubscription = this.discoverResources(selectedProvider).subscribe();
      return;
    }
    if (invalid) {
      this._error = undefined;
      this._submittingCredentialsForm = false;
      return;
    }
    const name = value?.credentials?.name;
    const accessKeyId = value?.credentials?.accessKey;
    const secretAccessKey = value?.credentials?.secretKey;
    const tag = value?.credentials?.tag;
    const provider: Provider = {
      aws: {},
      name: name
    };
    if (tag) {
      provider.tags = { [tag]: tag };
    }
    const credentials: AWSCredentials = {
      accessKeyId,
      secretAccessKey
    };
    this.createProvider(provider, { aws: credentials });
  }

  public submitAzureCredentialsForm() {
    const { value, invalid } = this._azureCredentialsForm;
    const selectedProvider = value?.provider;
    if (selectedProvider) {
      this._discoverResourcesSubscription?.unsubscribe();
      this._discoverResourcesSubscription = this.discoverResources(selectedProvider).subscribe();
      return;
    }
    if (invalid) {
      this._error = undefined;
      this._submittingCredentialsForm = false;
      return;
    }
    const subscription = value?.credentials?.subscription;
    const accessKey = value?.credentials?.accessKey;
    const secretKey = value?.credentials?.secretKey;
    const tenant = value?.credentials?.tenant;
    const provider: Provider = {
      azure: {},
      name: subscription
    };
    const credentials: AzureCredentials = {
      clientId: accessKey,
      clientSecret: secretKey,
      subscriptionId: subscription,
      tenantId: tenant
    };
    this.createProvider(provider, { azure: credentials });
  }

  private createProvider(provider: Provider, credentials: Credentials) {

    this._submittingCredentialsForm = true;

    this._discoverResourcesSubscription = this._providerService
      .postProvider(provider)
      .pipe(
        mergeMap(provider => this._providerService
          .putProviderCredentials(provider.identifier as string, credentials)
          .pipe(
            map(() => provider)
          )
        ),
        catchError((error: HttpErrorResponse) => {
          this._error = error.message;
          return EMPTY;
        }),
        finalize(() => {
          this._submittingCredentialsForm = false;
        }),
        mergeMap((provider: Provider) => this.discoverResources(provider))
      )
      .subscribe();
  }

  public isAllSelected(): boolean {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;

    return numSelected === numRows;
  }

  public masterToggle() {
    if (this.isAllSelected()) {
      this.selection.clear();

      return;
    }

    this.selection.select(...this.dataSource.data);
  }

  public checkboxLabel(row?: ESpecNetwork): string {
    if (!row) {
      return `${this.isAllSelected() ? 'deselect' : 'select'} all`;
    }

    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} ${row.name ?? ''} row`;
  }

  public onImportMore() {
    this._platform = undefined;
    this._provider = undefined;

    this._stepper?.reset();
  }

  public onDone() {
    const reqs$ = new Array<Observable<unknown>>();

    this._dataSource.data.forEach((network: ESpecNetwork) => {
      if (!network.identifier) {
        return;
      }

      if (this._selection.isSelected(network)) {
        reqs$.push(this._networkService.showNetwork(network.identifier));
      } else {
        reqs$.push(this._networkService.ignoreNetwork(network.identifier));
      }
    });

    if (reqs$.length) {
      forkJoin(reqs$).subscribe({
        next: () => this._router.navigate(['create-netspec'])
      });
    }
  }

  public get platform(): CloudPlatform | undefined {
    return this._platform;
  }

  public get isPlatformSelected(): boolean {
    return this._platform !== undefined;
  }

  public get isProviderCreated(): boolean {
    return this._provider !== undefined;
  }

  public get CloudPlatform(): typeof CloudPlatform {
    return CloudPlatform;
  }

  public get awsCredentialsForm(): IFormGroup<AwsProviderForm> {
    return this._awsCredentialsForm;
  }

  public get azureCredentialsForm(): IFormGroup<AzureCredentialsForm> {
    return this._azureCredentialsForm;
  }

  public get RESOURCE_TABLE_COLUMNS(): string[] {
    return RESOURCE_TABLE_COLUMNS;
  }

  public get dataSource(): MatTableDataSource<ESpecNetwork> {
    return this._dataSource;
  }

  public get selection(): SelectionModel<ESpecNetwork> {
    return this._selection;
  }

  public get error(): string | undefined {
    return this._error;
  }

  public set error(value: string | undefined) {
    this._error = value;
  }

  public get submittingCredentialsForm(): boolean {
    return this._submittingCredentialsForm;
  }

  public get providers$(): Observable<Provider[]> | undefined {
    return this._providers$;
  }

  public compareProviders(p1: Provider, p2: Provider): boolean {
    // TODO check why the 'Create new...' option is not selected by default without next code
    if (!p1 && !p2) {
      return true;
    }
    return p1 === p2;
  }
}
