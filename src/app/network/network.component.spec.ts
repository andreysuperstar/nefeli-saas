import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatCardModule } from '@angular/material/card';
import { By } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { Subject } from 'rxjs';
import { NetspecComponent } from '../netspec/netspec.component';

import { NetworkComponent } from './network.component';

describe('NetworkComponent', () => {
  let fixture: ComponentFixture<NetworkComponent>;
  const routeParamsSubject = new Subject();
  let httpTestingController: HttpTestingController;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HttpClientTestingModule,
        MatCardModule
      ],
      declarations: [
        NetworkComponent,
        NetspecComponent
      ],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            params: routeParamsSubject
          }
        }
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NetworkComponent);
    fixture.detectChanges();
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  it('should render specified netspec', () => {
    const netspecId = '713';

    routeParamsSubject.next({ netspecId });
    fixture.detectChanges();

    expect(fixture.debugElement.query(By.directive(NetspecComponent))).toBeDefined();
    httpTestingController.expectOne(`http://localhost/v1/netspecs/${netspecId}`);
  });
});
