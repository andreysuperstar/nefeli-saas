import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NetworkRoutingModule } from './network-routing.module';
import { NetworkComponent } from './network.component';
import { NetspecService } from '../netspec/netspec.service';
import { MatCardModule } from '@angular/material/card';
import { NetspecModule } from '../netspec/netspec.module';

@NgModule({
  declarations: [
    NetworkComponent
  ],
  imports: [
    CommonModule,
    NetspecModule,
    NetworkRoutingModule,
    MatCardModule
  ],
  providers: [
    NetspecService
  ]
})
export class NetworkModule { }
