import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'saas-network',
  templateUrl: './network.component.html',
  styleUrls: ['./network.component.less']
})
export class NetworkComponent implements OnInit {
  private _netspecId: string | undefined;

  constructor(private _route: ActivatedRoute) { }

  public ngOnInit() {
    this.handleRouteChanges();  
  }

  private handleRouteChanges() {
    this._route.params.subscribe({
      next: (p: Params) => this._netspecId = p['netspecId'] as string
    });
  }

  public get netspecId(): string | undefined {
    return this._netspecId;
  }
}
