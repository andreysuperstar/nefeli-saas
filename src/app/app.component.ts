import { Component } from '@angular/core';

@Component({
  selector: 'saas-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent { }
