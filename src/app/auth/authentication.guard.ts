import { Injectable } from '@angular/core';
import { CanLoad, Route, Router, UrlSegment } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationGuard implements CanLoad {
  /* TODO(eric): remove when authentication is implemented
  temporary var to simulate toggling a user sesssion */
  private _hasSession = true;

  constructor(private _router: Router) { }

  public canLoad(route: Route, segments: UrlSegment[]): boolean {
    if (!this._hasSession) {
      void this._router.navigate(['auth', 'login']);
    }

    return this._hasSession;
  }
}
