import { ComponentFixture, discardPeriodicTasks, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { NetspecComponent } from './netspec.component';
import { Netspec } from '../../../mock/data/netspecs';
import { getNetworkById } from '../../../mock/data/networks';
import { resources } from '../../../mock/data/resources';
import { cloneDeep } from 'lodash';
import { LinkStatus } from './elements/link.element';
import { ExternalNetworkSpecStatusResourceDeployStatusLifecycleState } from '../../../rest_client/saas/model/externalNetworkSpecStatusResourceDeployStatusLifecycleState';
import { LocalStorageService } from '../shared/local-storage.service';
import { ExternalNetworkSpec } from '../../../rest_client/saas/model/externalNetworkSpec';

describe('NetspecComponent', () => {
  let component: NetspecComponent;
  let fixture: ComponentFixture<NetspecComponent>;
  let http: HttpTestingController;
  let localStorageService: LocalStorageService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [LocalStorageService],
      declarations: [NetspecComponent]
    })
      .compileComponents();

    http = TestBed.inject(HttpTestingController);
    localStorageService = TestBed.inject(LocalStorageService);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NetspecComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    localStorageService.write('netspecRouter-1', JSON.stringify({ x: 0, y: 0 }));
    localStorageService.write('netspecNetwork-1', JSON.stringify({ x: 0, y: 0 }));
    localStorageService.write('netspecNetwork-2', JSON.stringify({ x: 0, y: 0 }));
    localStorageService.write('netspecNetwork-3', JSON.stringify({ x: 0, y: 0 }));
    localStorageService.write('netspecNetwork-4', JSON.stringify({ x: 0, y: 0 }));
  });

  it('should render provided netspec', () => {
    component.netspec = Netspec;
    component.ngAfterViewInit();
    fixture.detectChanges();

    handleNetspecGraphPoll();
    fixture.detectChanges();

    // TODO(eric): fix commented below, causing intermittent failures
    // const numNetworks = Netspec.config?.networkIds?.length;
    const el = fixture.debugElement.nativeNode;
    const nodeLabels = el.querySelectorAll('.node .label');

    // expect(nodeLabels.length).toBe(numNetworks);
    expect(nodeLabels[0].textContent).toBe('prod-vpc');
    expect(nodeLabels[1].textContent).toBe('dev-vpc');
    expect(nodeLabels[2].textContent).toBe('qa-vnet');
    expect(nodeLabels[3].textContent).toBe('engr-vnet');

    const nodeDescriptions = el.querySelectorAll('.node .description');
    // expect(nodeDescriptions.length).toBe(numNetworks);
    expect(nodeDescriptions[0].textContent).toBe('us-east-1');
    expect(nodeDescriptions[1].textContent).toBe('us-west-1');
    expect(nodeDescriptions[2].textContent).toBe('West Europe');
    expect(nodeDescriptions[3].textContent).toBe('West Europe');

    expect(el.querySelectorAll('.link').length).toBe(Netspec.config?.links?.length);
  });

  it('should fetch netspec when only id is provided', () => {
    component.netspecId = '123';
    component.ngAfterViewInit();

    handleNetspecGraphPoll();
  });

  it('should draw link between two nodes', () => {
    component.draggedResource = resources[0];
    component.onDrop({
      clientX: 100,
      clientY: 200
    } as any);

    component.draggedResource = resources[0];
    component.onDrop({
      clientX: 300,
      clientY: 400
    } as any);

    const networkNodes = fixture.debugElement.nativeNode.querySelectorAll('svg > svg');

    networkNodes[0].dispatchEvent(new Event('click'));
    networkNodes[1].dispatchEvent(new Event('click'));

    const el = fixture.debugElement.nativeNode
    expect(el.querySelector('svg .link')).toBeDefined();
    expect(el.querySelector('svg .link .router')).toBeDefined();
  });

  it('should create new logical router', () => {
    const el = fixture.debugElement.nativeNode;

    component.draggedResource = resources[0];
    component.onDrop({
      clientX: 100,
      clientY: 200
    } as any);

    component.draggedResource = resources[1];
    component.onDrop({
      clientX: 300,
      clientY: 400
    } as any);

    component['onNetworkNodeClick'](component['_graphElements']?.networks[0]);
    component['onNetworkNodeClick'](component['_graphElements']?.networks[1]);

    // verify just one link and temp router
    expect(el.querySelectorAll('.link').length).toBe(1);
    expect(el.querySelectorAll('.link .router').length).toBe(1);

    component.draggedResource = resources[2];
    component.onDrop({
      clientX: 200,
      clientY: 300
    } as any);

    const routerNode = el.querySelector('.link > svg');
    const newNetworkNode = el.querySelectorAll('svg > svg')[2];

    routerNode.dispatchEvent(new Event('click'));
    newNetworkNode.dispatchEvent(new Event('click'));

    // verify three links rendered, router element, and temp router removed
    expect(el.querySelectorAll('.link').length).toBe(3);
    expect(el.querySelector('svg > svg .router')).not.toBeNull();
    expect(el.querySelectorAll('.link .router').length).toBe(0);
  });

  const handleNetspecGraphPoll = (spec?: ExternalNetworkSpec) => {
    if (spec) {
      http.expectOne('http://localhost/v1/netspecs/123').flush(spec);
    }

    let networkId = Netspec.config?.networkIds?.[0] ?? '';
    http.match(`http://localhost:4200/v1/networks/${networkId}`).forEach((req) => req.flush(getNetworkById(networkId) ?? {}));
    networkId = Netspec.config?.networkIds?.[1] ?? '';
    http.match(`http://localhost:4200/v1/networks/${networkId}`).forEach((req) => req.flush(getNetworkById(networkId) ?? {}));
    networkId = Netspec.config?.networkIds?.[2] ?? '';
    http.match(`http://localhost:4200/v1/networks/${networkId}`).forEach((req) => req.flush(getNetworkById(networkId) ?? {}));
    networkId = Netspec.config?.networkIds?.[3] ?? '';
    http.match(`http://localhost:4200/v1/networks/${networkId}`).forEach((req) => req.flush(getNetworkById(networkId) ?? {}));
  }

  it('should update status of links', fakeAsync(() => {
    component.netspecId = '123';
    component.ngAfterViewInit();

    // PENDING_CREATION = pending state
    const spec = cloneDeep(Netspec);
    handleNetspecGraphPoll(spec);
    expect(component['_graphElements'].links[0].status).toBe(LinkStatus.PENDING);

    tick(10000);

    if (!spec?.status?.resourceDeployStatuses) {
      expect(false).withContext('netspec does not have resource deploy status').toBe(true);
      return;
    }

    // REQUIRES_ATTENTION = error state
    spec.status.resourceDeployStatuses['2'].state = ExternalNetworkSpecStatusResourceDeployStatusLifecycleState.RequiresAttention;
    handleNetspecGraphPoll(spec);
    expect(component['_graphElements'].links[0].status).toBe(LinkStatus.ERROR);

    tick(10000);

    // DEPLOYED = deploy state
    spec.status.resourceDeployStatuses['2'].state = ExternalNetworkSpecStatusResourceDeployStatusLifecycleState.Deployed;
    handleNetspecGraphPoll(spec);
    expect(component['_graphElements'].links[0].status).toBe(LinkStatus.DEPLOYED);

    tick(10000);

    // CREATING = pending state
    spec.status.resourceDeployStatuses['2'].state = ExternalNetworkSpecStatusResourceDeployStatusLifecycleState.Creating;
    handleNetspecGraphPoll(spec);
    expect(component['_graphElements'].links[0].status).toBe(LinkStatus.PENDING);

    tick(10000);

    // CREATION_FAILED = error state
    spec.status.resourceDeployStatuses['2'].state = ExternalNetworkSpecStatusResourceDeployStatusLifecycleState.CreationFailed;
    handleNetspecGraphPoll(spec);
    expect(component['_graphElements'].links[0].status).toBe(LinkStatus.ERROR);

    tick(10000);

    // PENDING_REMOVAL = pending state
    spec.status.resourceDeployStatuses['2'].state = ExternalNetworkSpecStatusResourceDeployStatusLifecycleState.PendingRemoval;
    handleNetspecGraphPoll(spec);
    expect(component['_graphElements'].links[0].status).toBe(LinkStatus.PENDING);

    tick(10000);

    // REMOVAL_FAILED = error state
    spec.status.resourceDeployStatuses['2'].state = ExternalNetworkSpecStatusResourceDeployStatusLifecycleState.RemovalFailed;
    handleNetspecGraphPoll(spec);
    expect(component['_graphElements'].links[0].status).toBe(LinkStatus.ERROR);

    tick(10000);

    // REMOVING = pending state
    spec.status.resourceDeployStatuses['2'].state = ExternalNetworkSpecStatusResourceDeployStatusLifecycleState.Removing;
    handleNetspecGraphPoll(spec);
    expect(component['_graphElements'].links[0].status).toBe(LinkStatus.PENDING);

    tick(10000);

    discardPeriodicTasks();
  }));

  it('should save network and router position', () => {
    const el = fixture.debugElement.nativeNode;
    component.netspec = Netspec;
    component.ngAfterViewInit();

    let networkId = Netspec.config?.networkIds?.[0] ?? '';
    http.expectOne(`http://localhost:4200/v1/networks/${networkId}`).flush(getNetworkById(networkId) ?? {});
    networkId = Netspec.config?.networkIds?.[1] ?? '';
    http.expectOne(`http://localhost:4200/v1/networks/${networkId}`).flush(getNetworkById(networkId) ?? {});
    networkId = Netspec.config?.networkIds?.[2] ?? '';
    http.expectOne(`http://localhost:4200/v1/networks/${networkId}`).flush(getNetworkById(networkId) ?? {});
    networkId = Netspec.config?.networkIds?.[3] ?? '';
    http.expectOne(`http://localhost:4200/v1/networks/${networkId}`).flush(getNetworkById(networkId) ?? {});
    fixture.detectChanges();

    // drop network and check saving position
    component.draggedResource = resources[0];
    component.onDrop({
      clientX: 500,
      clientY: 500
    } as any);

    let networkStoredPosition = JSON.parse(localStorageService.read('netspecNetwork-1') ?? '{}');
    expect(networkStoredPosition?.x).toBe(500);
    // the value is dynamic and depends to the svg object state
    expect(networkStoredPosition?.y).not.toBeNull();
    expect(networkStoredPosition?.y).toBeDefined();
    expect(networkStoredPosition?.y).not.toBe(0);


    component.draggedResource = resources[1];
    component.onDrop({
      clientX: 300,
      clientY: 400
    } as any);
    networkStoredPosition = JSON.parse(localStorageService.read('netspecNetwork-2') ?? '{}');
    expect(networkStoredPosition?.x).toBe(300);
    expect(networkStoredPosition?.y).not.toBeNull();
    expect(networkStoredPosition?.y).toBeDefined();
    expect(networkStoredPosition?.y).not.toBe(0);

    // link networks
    component['onNetworkNodeClick'](component['_graphElements']?.networks[0]);
    component['onNetworkNodeClick'](component['_graphElements']?.networks[1]);

    // add logical router
    component.draggedResource = resources[2];
    component.onDrop({
      clientX: 200,
      clientY: 300
    } as any);

    const routerNode = el.querySelector('.link > svg');
    const newNetworkNode = el.querySelectorAll('svg > svg')[2];
    routerNode.dispatchEvent(new Event('click'));
    newNetworkNode.dispatchEvent(new Event('click'));

    const logicalRouterIdentifier = component['_netspec']?.config?.routers?.[1]?.identifier;
    const routerStoredPosition = JSON.parse(localStorageService.read(`netspecRouter-${logicalRouterIdentifier ?? ''}`) ?? '{}');
    expect(routerStoredPosition?.x).toBe(8);
    expect(routerStoredPosition?.y).toBe(8);

  });
});
