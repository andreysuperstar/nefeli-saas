import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NetspecComponent } from './netspec.component';



@NgModule({
  declarations: [ NetspecComponent ],
  imports: [
    CommonModule
  ],
  exports: [ NetspecComponent ]
})
export class NetspecModule { }
