import { AfterViewInit, Component, Input } from '@angular/core';
import { G, Matrix, SVG, Svg } from '@svgdotjs/svg.js';
import { ESpecLink } from '../../../rest_client/saas/model/eSpecLink';
import { ESpecLogicalRouter } from '../../../rest_client/saas/model/eSpecLogicalRouter';
import { ExternalNetworkSpec } from '../../../rest_client/saas/model/externalNetworkSpec';
import { ESpecNetwork } from '../../../rest_client/saas/model/eSpecNetwork';
import { ResourceType, Coords } from './elements/elements';
import { LinkElement, LinkStatus } from './elements/link.element';
import { NodeElement, NodeElementState } from './elements/node.element';
import { RouterElement } from './elements/router.element';
import { NetspecGraph, NetspecService, NetworkNode } from './netspec.service';
import { ExternalNetworkSpecStatus } from '../../../rest_client/saas/model/externalNetworkSpecStatus';
import { ExternalNetworkSpecStatusResourceDeployStatusLifecycleState } from '../../../rest_client/saas/model/externalNetworkSpecStatusResourceDeployStatusLifecycleState';
import { LocalStorageService, LocalStorageKey } from '../shared/local-storage.service';

interface GraphElements {
  links: LinkElement[];
  networks: NodeElement[];
  routers: RouterElement[];
}
@Component({
  selector: 'saas-netspec',
  templateUrl: './netspec.component.html',
  styleUrls: ['./netspec.component.less']
})
export class NetspecComponent implements AfterViewInit {
  private _svg: Svg | undefined;
  private _linksSvgGroup: G | undefined;
  private _netspecId: string | undefined;
  private _netspec: ExternalNetworkSpec | undefined;
  private _draggedResource: ESpecNetwork | undefined;
  private _drawnLink: LinkElement | undefined;
  private _graphRendered = false;
  private _graphElements: GraphElements = { links: [], networks: [], routers: [] };

  @Input()
  public set netspecId(id: string | undefined) {
    this._netspecId = id;
  }

  @Input()
  public set netspec(val: ExternalNetworkSpec | undefined) {
    this._netspec = val;
  }

  constructor(
    private _netspecService: NetspecService,
    private _localStorageService: LocalStorageService
  ) { }

  public ngAfterViewInit() {
    this._svg = SVG().addTo('#canvas').size('100%', '100%');
    this._linksSvgGroup = this._svg.group();

    if (this._netspec) {
      this._netspecService.convertToGraph(this._netspec).subscribe({
        next: (graph: NetspecGraph) => this.renderGraph(graph)
      });
    } else if (this._netspecId && !this._netspec) {
      this._netspecService.streamNetspecGraph(this._netspecId).subscribe((graph: NetspecGraph | undefined) => {
        if (graph) {
          // _graphRendered is temporary until support live updating of netspec
          if (!this._graphRendered) {
            this.renderGraph(graph);
            this._graphRendered = true;
          }

          if (graph.status) {
            this.updateStatus(graph.status);
          }
        }
      });
    }
  }

  private updateStatus(status: ExternalNetworkSpecStatus) {
    this.updateLinksStatus(status);
  }

  private updateLinksStatus({ resourcesByNetspecElement, resourceDeployStatuses }: ExternalNetworkSpecStatus) {
    this._graphElements.links.forEach((link: LinkElement) => {
      if (!link.settings || !resourcesByNetspecElement) {
        return;
      }

      const { identifier } = link.settings;

      if (identifier) {
        const resources = resourcesByNetspecElement[identifier];
        let linkStatus = LinkStatus.NONE;

        resources.concreteCloudResourceIds?.forEach((concreteCloudResourceId: string) => {
          if (!resourceDeployStatuses) {
            return;
          }
          const status = resourceDeployStatuses[concreteCloudResourceId];

          switch (status.state) {
            case ExternalNetworkSpecStatusResourceDeployStatusLifecycleState.PendingCreation:
            case ExternalNetworkSpecStatusResourceDeployStatusLifecycleState.Creating:
            case ExternalNetworkSpecStatusResourceDeployStatusLifecycleState.PendingRemoval:
            case ExternalNetworkSpecStatusResourceDeployStatusLifecycleState.Removing:
              linkStatus = linkStatus < LinkStatus.PENDING ? LinkStatus.PENDING : linkStatus;
              break;
            case ExternalNetworkSpecStatusResourceDeployStatusLifecycleState.SummaryNull:
            case ExternalNetworkSpecStatusResourceDeployStatusLifecycleState.CreationFailed:
            case ExternalNetworkSpecStatusResourceDeployStatusLifecycleState.RequiresAttention:
            case ExternalNetworkSpecStatusResourceDeployStatusLifecycleState.RemovalFailed:
              linkStatus = linkStatus < LinkStatus.ERROR ? LinkStatus.ERROR : linkStatus;
              break;
            case ExternalNetworkSpecStatusResourceDeployStatusLifecycleState.Deployed:
              linkStatus = linkStatus < LinkStatus.DEPLOYED ? LinkStatus.DEPLOYED : linkStatus;
              break;
          }
        });

        link.status = linkStatus;
      }
    });
  }

  private renderGraph(graph: NetspecGraph) {
    const networks = Array.from(graph.local?.networks?.values() ?? []);
    const routers = Array.from(graph.config?.routers ?? []);
    if (networks.length > 1) {
      this.renderRouters(routers, graph);
      this.renderNetworks(networks);
      this.renderLinks(graph);
    }
  }

  private renderRouters(routers: ESpecLogicalRouter[], graph: NetspecGraph) {
    let coords: Coords;

    routers.forEach(({ identifier }: ESpecLogicalRouter) => {
      if (!this._svg || !identifier) {
        return;
      }
      try {
        coords = JSON.parse(this._localStorageService.read(this.getStoreName(LocalStorageKey.NETSPEC_ROUTER, identifier)) ?? '{}') as Coords;
      } catch (e) {
        // TODO (anton): add logging when get implemented.
      }

      const element = new RouterElement(
        this._svg,
        coords,
        { identifier }
      );

      graph.local?.routerElements?.set(identifier, element);
      this._graphElements.routers.push(element);
    })
  }

  private renderNetworks(networks: NetworkNode[]) {
    let coords: Coords;

    networks.forEach((network: NetworkNode) => {
      if (!this._svg) {
        return;
      }

      try {
        coords = JSON.parse(this._localStorageService.read(this.getStoreName(LocalStorageKey.NETSPEC_NETWORK, network.identifier ?? '{}')) ?? '') as Coords;
      } catch (e) {
        // TODO (anton): add logging when get implemented.
      }

      const type = network.aws ? ResourceType.AWS :
        network.azure ? ResourceType.AZURE :
          network.gcp ? ResourceType.GCP : undefined;

      const description = type === ResourceType.AWS ? network.aws?.region :
        type === ResourceType.AZURE ? network.azure?.region : '';

      const node = new NodeElement(
        this._svg,
        {
          label: network.name ?? '',
          description,
          type,
          identifier: network.identifier
        },
        network.local.position ?? coords,
        NodeElementState.DEPLOYED
      );

      network.local.element = node;
      this._graphElements.networks.push(node);

      node.svg.on('click', () => this.onNetworkNodeClick(node));
    });
  }

  private onNetworkNodeClick(node: NodeElement | RouterElement) {
    if (!this._linksSvgGroup) {
      return;
    }

    if (this._drawnLink) {
      const srcNode = this._drawnLink.src;

      if (srcNode !== node) {
        this.createNewLink(srcNode, node);
      }

      this._drawnLink.remove();
      this._drawnLink = undefined;


    } else {
      this._drawnLink = new LinkElement(this._linksSvgGroup, node);
    }
  }

  private createNewLink(srcNode: NodeElement | RouterElement, dstNode: NodeElement | RouterElement) {
    if (!this._linksSvgGroup) {
      return;
    }

    let addRouter = false;
    let routerNode: RouterElement | undefined;

    [srcNode, dstNode].forEach((node: NodeElement | RouterElement) => {
      if (node instanceof NodeElement) {
        node.unselect();
      } else if (node instanceof RouterElement) {
        if (node.tempLine) {
          addRouter = true;
        }
        routerNode = node;
      }
    });

    if (addRouter && routerNode?.tempLine) {
      const newRouter = this.addRouterToLink(routerNode.tempLine);

      if (newRouter && srcNode instanceof RouterElement) {
        srcNode = newRouter;
      } else if (newRouter && dstNode instanceof RouterElement) {
        dstNode = newRouter;
      }
    }

    const newLink = new LinkElement(
      this._linksSvgGroup,
      srcNode,
      dstNode,
      { showRouter: dstNode instanceof NodeElement && srcNode instanceof NodeElement }
    );
    newLink.linkGroup?.back();

    const router = newLink.router;
    router?.svg.on('click', () => this.onNetworkNodeClick(router));

    this._netspec?.config?.links?.push(
      this._netspecService.createLink(srcNode, dstNode)
    );
  }

  private addRouterToLink(link: LinkElement): RouterElement | undefined {
    const srcNode = link.src;
    const dstNode = link.dst;
    const center = link.center;

    if (!this._svg || !dstNode) {
      return;
    }

    this.removeLink(link);

    const routerId = Math.random().toString();
    const router = new RouterElement(
      this._svg,
      center,
      { identifier: routerId }
    );
    router?.svg.on('click', () => this.onNetworkNodeClick(router));

    this._netspec?.config?.routers?.push({
      identifier: routerId,
      name: `router ${routerId}`
    });

    this.createNewLink(srcNode, router);
    this.createNewLink(router, dstNode);

    this._localStorageService.write(this.getStoreName(LocalStorageKey.NETSPEC_ROUTER, routerId), JSON.stringify({
      ...center
    }));

    return router;
  }

  private removeLink(link: LinkElement) {
    const removeIndex = this._netspec?.config?.links?.findIndex((specLink: ESpecLink) =>
      (specLink.nodeA?.networkIdentifier === link.src.settings?.identifier
        || specLink.nodeA?.logicalRouterIdentifier === link.src.settings?.identifier)
      && (specLink.nodeB?.networkIdentifier === link.dst?.settings?.identifier
        || specLink.nodeB?.logicalRouterIdentifier === link.dst?.settings?.identifier)
    );

    if (removeIndex !== undefined && removeIndex >= 0) {
      this._netspec?.config?.links?.splice(removeIndex, 1);
    }

    link.remove();
  }

  private renderLinks(graph: NetspecGraph) {
    const links = graph.config?.links ?? [];
    const networks = graph.local?.networks;
    const routerElements = graph.local?.routerElements;

    links.forEach((link: ESpecLink) => {
      if (!this._linksSvgGroup) {
        return;
      }

      const networkNodeA = link.nodeA?.networkIdentifier ?
        networks?.get(link.nodeA?.networkIdentifier)?.local.element :
        link.nodeA?.logicalRouterIdentifier ?
          routerElements?.get(link.nodeA?.logicalRouterIdentifier) : undefined;

      const networkNodeB = link.nodeB?.networkIdentifier ?
        networks?.get(link.nodeB?.networkIdentifier)?.local.element :
        link.nodeB?.logicalRouterIdentifier ?
          routerElements?.get(link.nodeB?.logicalRouterIdentifier) : undefined;

      if (!networkNodeA || !networkNodeB) {
        return;
      }

      const linkEl = new LinkElement(
        this._linksSvgGroup,
        networkNodeA,
        networkNodeB,
        { identifier: link.identifier }
      );
      this._graphElements.links.push(linkEl);
    });

    this._linksSvgGroup?.back();
  }


  private getStoreName(keyType: LocalStorageKey, itemId: string): string {
    return `${keyType}-${itemId}`;
  }

  public onDrop(event: DragEvent) {
    if (!this._svg || !this._draggedResource) {
      return;
    }

    const point = this._svg.point(event.clientX, event.clientY);
    const matrix = new Matrix(this._svg);
    point.transform(matrix.inverse());

    const network = this._netspecService.convertToNetworkNode(this._draggedResource);
    network.local.position = { x: point.x, y: point.y };
    this._localStorageService.write(this.getStoreName(LocalStorageKey.NETSPEC_NETWORK, network.identifier ?? ''), JSON.stringify({
      ...network.local.position
    }));
    this.renderNetworks([network])

    if (!this._netspec) {
      this._netspec = {
        config: {
          name: 'Netspec #1',
          networkIds: [],
          routers: [],
          links: []
        }
      };
    }

    if (network.identifier) {
      this._netspec.config?.networkIds?.push(network.identifier);
    }
  }

  public allowDrop(event: Event) {
    event.preventDefault();
    event.stopPropagation();
  }

  public set draggedResource(val: ESpecNetwork) {
    this._draggedResource = val;
  }

  public get netspecId(): string | undefined {
    return this._netspecId;
  }

  public get netspec(): ExternalNetworkSpec | undefined {
    return this._netspec;
  }
}
