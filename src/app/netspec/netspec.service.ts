import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { NetSpecsService } from '../../../rest_client/saas/api/netSpecs.service';
import { ESpecNetwork } from '../../../rest_client/saas/model/eSpecNetwork';
import { ExternalNetworkSpec } from '../../../rest_client/saas/model/externalNetworkSpec';
import { catchError, concat, EMPTY, forkJoin, map, mapTo, mergeMap, Observable, of, repeat, shareReplay, skip, timer } from 'rxjs';
import { environment } from '../../environments/environment';
import { NodeElement } from './elements/node.element';
import { RouterElement } from './elements/router.element';
import { ExternalNetworkSpecConfig } from '../../../rest_client/saas/model/externalNetworkSpecConfig';
import { Coords } from './elements/elements';
import { ESpecLink } from '../../../rest_client/saas/model/eSpecLink';

export interface NetworkNode extends ESpecNetwork {
  local: {
    element?: NodeElement;
    position?: Coords;
  }
}

export interface NetspecGraph extends ExternalNetworkSpec {
  local?: {
    networks?: Map<string, NetworkNode>;
    routerElements?: Map<string, RouterElement>;
  }
}

@Injectable({
  providedIn: 'root'
})
export class NetspecService {
  constructor(
    private _restNetspecsService: NetSpecsService,
    private _http: HttpClient
  ) { }

  public getNetspec(id: string, status?: boolean): Observable<ExternalNetworkSpec> {
    return this._restNetspecsService.getNetSpec(id, status);
  }

  public getNetspecGraph(id: string): Observable<NetspecGraph> {
    return this._restNetspecsService.getNetSpec(id).pipe(
      mergeMap((spec: ExternalNetworkSpec) => this.convertToGraph(spec))
    );
  }

  public streamNetspecGraph(id: string): Observable<NetspecGraph | undefined> {
    const pollingInterval = 10000;
    return concat(
      this.getNetspecGraph(id),
      timer(pollingInterval).pipe(
        mapTo(undefined),
        skip(1)
      )
    ).pipe(
      repeat(),
      shareReplay({
        refCount: true
      })
    );
  }

  public convertToNetworkNode({ aws, azure, gcp, identifier, name }: ESpecNetwork): NetworkNode {
    return { aws, azure, gcp, identifier, name, local: {} }
  }

  public convertToGraph(spec: ExternalNetworkSpec): Observable<NetspecGraph> {
    const networkRequests = spec.config?.networkIds?.map((id: string): Observable<ESpecNetwork> =>
      this._http.get<ESpecNetwork>(`${environment.restServer}/v1/networks/${id}`));

    return forkJoin({
      spec: of(spec),
      networks: forkJoin(networkRequests ?? [])
    }).pipe(
      map(({ spec, networks }: {
        spec: ExternalNetworkSpec,
        networks: ESpecNetwork[]
      }) => {
        const networksMap = new Map<string, NetworkNode>();
        const routerElements = new Map<string, RouterElement>();

        networks.forEach((network: ESpecNetwork) => {
          if (network.identifier) {
            networksMap.set(network.identifier, {
              ...network,
              local: {}
            });
          }
        });

        return {
          ...spec,
          local: {
            networks: networksMap,
            routerElements
          }
        }
      })
    );
  }

  public createLink(srcNode: NodeElement | RouterElement, dstNode: NodeElement | RouterElement): ESpecLink {
    const link: ESpecLink = {
      nodeA: {
        networkIdentifier: srcNode instanceof NodeElement ? srcNode.settings.identifier : undefined,
        logicalRouterIdentifier: srcNode instanceof RouterElement ? srcNode.settings?.identifier : undefined
      },
      nodeB: {
        networkIdentifier: dstNode instanceof NodeElement ? dstNode.settings.identifier : undefined,
        logicalRouterIdentifier: dstNode instanceof RouterElement ? dstNode.settings?.identifier : undefined
      }
    };

    return link;
  }

  public applyNetspec(netspec: ExternalNetworkSpecConfig): Observable<ExternalNetworkSpec> {

    // workaround until support versioning, see NEF-8593
    netspec.version = '1';

    return this._restNetspecsService.postNetSpec(netspec).pipe(
      mergeMap(({ identifier }: ExternalNetworkSpecConfig) => {
        return identifier ? this._restNetspecsService.getNetSpec(identifier, true) : EMPTY;
      }),
      catchError(() => EMPTY)
    );
  }

  public approveNetspec(netspecId: string, revision: string): Observable<unknown> {
    return this._restNetspecsService.approveNetSpec(netspecId, revision);
  }
}