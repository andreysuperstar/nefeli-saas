export interface Coords {
  x: number;
  y: number;
}

export enum ResourceType {
  NONE,
  AWS,
  AZURE,
  GCP
}
