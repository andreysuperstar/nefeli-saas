import '@svgdotjs/svg.filter.js';
import { G, Tspan, Svg, Filter } from '@svgdotjs/svg.js';
import { Coords, ResourceType } from './elements';

export const Config = {
  size: 100,
  padding: 8,
  gaussianBlur: 3,
  icon: {
    width: 25,
    height: 25,
    opacity: 0.5,
    cy: 25
  },
  onLoadAnimateDuration: 2000
};

export enum NodeElementState {
  SUMMARY_NULL,
  PENDING,
  IN_PROGRESS,
  DEPLOYED,
  REQUIRES_ATTENTION
}

export const StateColor = new Map([
  [NodeElementState.SUMMARY_NULL, 'red'],
  [NodeElementState.PENDING, 'yellow'],
  [NodeElementState.IN_PROGRESS, 'green'],
  [NodeElementState.DEPLOYED, 'green'],
  [NodeElementState.REQUIRES_ATTENTION, 'red']
]);

export const ResourceTypeImage = new Map([
  [ResourceType.AWS, 'assets/aws.png'],
  [ResourceType.AZURE, 'assets/azure.svg'],
  [ResourceType.GCP, 'assets/gcp.png']
]);

export interface NodeElementSettings {
  label: string;
  description?: string;
  type?: ResourceType;
  identifier?: string;
}

export class NodeElement {
  private _svg: Svg;

  constructor(
    private _root: Svg,
    private _settings: NodeElementSettings,
    private _position: Coords = { x: 0, y: 0 },
    private _state = NodeElementState.SUMMARY_NULL
  ) {
    this._svg = this._root.nested();
    this.render();
    this.setupHandlers();

    const { x, y } = this._position;
    this._svg.center(
      x - (+this._svg.bbox().width / 2),
      y - (+this._svg.bbox().height / 2)
    );
  }

  private setupHandlers() {
    this._svg.on('click', () => this.onClick() );
  }

  private onClick() {
    if (!this._svg) {
      return;
    }

    this._svg.findOne('circle')?.toggleClass('selected');
  }

  private render() {
    const group = this._svg.group();
    this.createNode(
      group,
      this._settings.label,
      this._settings.description,
      this._settings.type
    );
  }

  private createNode(
    root: G,
    label: string,
    description: string | undefined,
    type: ResourceType | undefined
  ): G {
    const group = root.group();
    group.attr({
      opacity: 0
    });

    const circle = group.circle(Config.size);
    circle
      .dx(Config.padding)
      .dy(Config.padding)
      .addClass(StateColor.get(this._state) ?? '');

    const text = group.text((add: Tspan) => {
      add.tspan(label).newLine().addClass('label');

      if (description) {
        add.tspan(description).newLine().addClass('description');
      }
    });

    const cy = circle.cy() + +text.height() / 2;
    const cx = circle.cx() - +text.width() / 2;

    text.ay(cy.toString());
    text.ax(cx.toString());

    if (type) {
      const img = group.image(ResourceTypeImage.get(type)).attr(Config.icon);
      img.cx(circle.cx());
      img.cy(Config.icon.cy);
    }

    group.addClass('node');

    // apply drop shadow
    const filter = function (this: G, add: Filter) {
      const blur = add
        .offset(0, 0)
        .in(add.$sourceAlpha)
        .gaussianBlur(Config.gaussianBlur, Config.gaussianBlur);
      add.blend(add.$source, blur, 'normal');
      this.size('200%', '200%').move('-50%', '-50%');
    };
    const hoverFilter = function (this: G, add: Filter) {
      const blur = add
        .offset(0, 0)
        .gaussianBlur(Config.gaussianBlur, Config.gaussianBlur);
      add.blend(add.$source, blur, 'normal');
      this.size('200%', '200%').move('-50%', '-50%');
    };
    group.filterWith(filter);

    group.on('mouseenter', (e) => {
      group.filterWith(hoverFilter);
    });
    group.on('mouseleave', (e) => {
      group.filterWith(filter);
    });

    group.animate(Config.onLoadAnimateDuration, 0, 'now').attr({
      opacity: 1
    });

    return group;
  }

  public unselect() {
    this._svg.findOne('circle')?.removeClass('selected');
  }

  public get settings(): NodeElementSettings {
    return this._settings;
  }

  public get svg(): Svg {
    return this._svg;
  }
}
