import { SVG } from '@svgdotjs/svg.js';
import { Coords, ResourceType } from './elements';
import { NodeElement, Config, StateColor, NodeElementState, ResourceTypeImage } from './node.element';

describe('NodeElement', () => {
  const svg = SVG().addTo('body').size('100%', '100%');

  const renderNode = (label: string, description: string, position: Coords): NodeElement => {
    
    return new NodeElement(svg,
      {
        label,
        description,
        type: ResourceType.AZURE
      },
      position,
      NodeElementState.DEPLOYED);
  };

  it('should render', () => {
    const position: Coords = { x: 100, y: 200 };
    const label = 'My Label';
    const description = 'My Description';
    const node = renderNode(label, description, position);
    const el: SVGElement = node.svg.node;

    expect(+node.svg.x() + (node.svg.bbox().width / 2)).toBe(position.x);
    expect(+node.svg.y() + (node.svg.bbox().height / 2)).toBe(position.y);

    const circle: SVGCircleElement | null = el.querySelector('.node > circle');
    expect(circle?.r.baseVal.value).toBe(Config.size / 2);
    expect(circle?.cx.baseVal.value).toBe(Config.size / 2 + Config.padding);
    expect(circle?.cy.baseVal.value).toBe(Config.size / 2 + Config.padding);
    expect(circle?.classList).toContain(StateColor.get(NodeElementState.DEPLOYED));

    const image: SVGImageElement | null = el.querySelector('.node > image');
    expect(image?.href.baseVal).toBe(ResourceTypeImage.get(ResourceType.AZURE));

    const labelEl: SVGTSpanElement | null = el.querySelector('.node > text > .label');
    expect(labelEl?.textContent).toBe(label);

    const descriptionEl: SVGTSpanElement | null = el.querySelector('.node > text > .description');
    expect(descriptionEl?.textContent).toBe(description);
  });

  it('should toggle selected state', () => {
    const node = renderNode('My Label', 'My Description', { x: 100, y: 200 });
    expect(node.svg.findOne('circle')?.hasClass('selected')).toBe(false);
    node.svg.dispatchEvent(new Event('click'));
    expect(node.svg.findOne('circle')?.hasClass('selected')).toBe(true);
    node.svg.dispatchEvent(new Event('click'));
    expect(node.svg.findOne('circle')?.hasClass('selected')).toBe(false);
  });
});
