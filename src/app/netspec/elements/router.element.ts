import '@svgdotjs/svg.filter.js';
import { G, Svg, Filter } from '@svgdotjs/svg.js';
import { Coords } from './elements';
import { LinkElement } from './link.element';

export const Config = {
  outer: {
    size: 20
  },
  inner: {
    size: 15
  },
  blur: 3
}

export interface RouterElementSettings {
  identifier?: string;
}

export class RouterElement {
  private _svg: Svg;
  private _tempLine?: LinkElement;

  constructor(
    private _root: Svg | G,
    private _position: Coords,
    private _settings?: RouterElementSettings
  ) {
    this._svg = this._root.nested();
    this.render();
  }

  private render() {
    const group = this._svg.group();

    const pointGroup = group.group();
    pointGroup.addClass('router');

    pointGroup
      .circle(Config.outer.size)
      .addClass('outer-circle');

    pointGroup
      .circle(Config.inner.size)
      .addClass('inner-circle')
      .center(pointGroup.cx(), pointGroup.cy());

    pointGroup.center(this._position?.x ?? 0, this._position?.y ?? 0);

    const hoverFilter = function (this: G, add: Filter) {
      const blur = add.offset(0, 0).gaussianBlur(Config.blur, Config.blur);
      add.blend(add.$source, blur, 'normal');
      this.size('200%', '200%').move('-50%', '-50%');
    };

    pointGroup.on('mouseenter', (e) => {
      pointGroup.filterWith(hoverFilter);
    });
    pointGroup.on('mouseleave', (e) => {
      pointGroup.unfilter();
    });
  }

  public set tempLine(val: LinkElement | undefined) {
    this._tempLine = val;
  }

  public get settings(): RouterElementSettings | undefined {
    return this._settings;
  }

  public get svg(): Svg {
    return this._svg;
  }

  public get tempLine(): LinkElement | undefined {
    return this._tempLine;
  }
}