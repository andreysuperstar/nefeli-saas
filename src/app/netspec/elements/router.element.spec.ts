import { SVG } from '@svgdotjs/svg.js';
import { Coords } from './elements';
import { RouterElement, Config } from './router.element';

describe('RouterElement', () => {
  const svg = SVG().addTo('body').size('100%', '100%');
  it('should render', () => {
    const pos: Coords = { x: 100, y: 200 };
    const router = new RouterElement(svg, pos);

    const innerCircle: SVGCircleElement | null = router.svg.node.querySelector('.router > circle.inner-circle');
    expect(innerCircle?.cx.baseVal.value).toBe(pos.x);
    expect(innerCircle?.cy.baseVal.value).toBe(pos.y);
    expect(innerCircle?.r.baseVal.value).toBe(Config.inner.size/2);

    const outerCircle: SVGCircleElement | null = router.svg.node.querySelector('.router > circle.outer-circle');
    expect(outerCircle?.cx.baseVal.value).toBe(pos.x);
    expect(outerCircle?.cy.baseVal.value).toBe(pos.y);
    expect(outerCircle?.r.baseVal.value).toBe(Config.outer.size/2);
  });
});
