import { G, Line, Svg, Filter } from '@svgdotjs/svg.js';
import { Coords } from './elements';
import { NodeElement } from './node.element';
import { RouterElement } from './router.element';

export const Config = {
  errorIcon: {
    outer: {
      size: 20
    },
    inner: {
      size: 15
    },
    blur: 2
  },
  pendingLink: {
    animation: {
      duration: 1000,
      delay: 100,
      when: 'now',
      swing: true,
      // TODO(eric): figure out how to animate indefinitely
      times: 999999999,
      wait: 20
    },
    opacity: 0.2
  }
}

export interface LinkSettings {
  identifier?: string;
  showRouter?: boolean;
}

export enum LinkStatus {
  NONE,
  DEPLOYED,
  PENDING,
  ERROR
}

export class LinkElement {
  private _linkGroup: G | undefined;
  private _lineEl: Line | undefined;
  private _router: RouterElement | undefined;
  private _status = LinkStatus.NONE;

  constructor(
    private _rootGroup: G,
    private _src: NodeElement | RouterElement,
    private _dst?: NodeElement | RouterElement,
    private _settings?: LinkSettings) {
    this.render();

    if (!this._dst) {
      this.followMouse();
    }
  }

  private render() {
    [this._linkGroup, this._lineEl] = this.createLink(this._rootGroup, this._src.svg, this._dst?.svg);

    if (this._settings?.showRouter) {
      this._router = this.addRouter(this._linkGroup, this._lineEl);
      this._router.tempLine = this;
    }
  }

  private renderPendingLink() {
    if (!this._dst?.svg) {
      return;
    }

    [this._linkGroup, this._lineEl] = this.createPendingLink(this._rootGroup, this._src.svg, this._dst.svg);
  }

  private renderErrorLink() {
    if (!this._dst?.svg) {
      return;
    }

    [this._linkGroup, this._lineEl] = this.createErrorLink(this._rootGroup, this._src.svg, this._dst.svg);
  }

  private createLink(
    root: G,
    src: Svg,
    dst?: Svg
  ): [G, Line] {
    const group = root.group();
    const destination = dst ?? src;

    const line = group.line(
      +src.x() + src.bbox().cx,
      +src.y() + src.bbox().cy,
      +destination.x() + destination.bbox().cx,
      +destination.y() + destination.bbox().cy
    );

    group.addClass('link');

    return [group, line];
  }

  private createPendingLink(root: G, src: Svg, dst: Svg): [G, Line] {
    const group = root.group();

    const line = group.line(
      +src.x() + src.bbox().cx,
      +src.y() + src.bbox().cy,
      +dst.x() + dst.bbox().cx,
      +dst.y() + dst.bbox().cy
    );

    line
      .animate(Config.pendingLink.animation)
      .attr({
        opacity: Config.pendingLink.opacity
      })
      .reverse();

    group.addClass('pending-link');

    return [group, line];
  }

  private createErrorLink(root: G, src: Svg, dst: Svg): [G, Line] {
    const group = root.group();

    const line = group.line(
      +src.x() + src.bbox().cx,
      +src.y() + src.bbox().cy,
      +dst.x() + dst.bbox().cx,
      +dst.y() + dst.bbox().cy
    );

    const pointGroup = group.group();
    pointGroup.addClass('error-icon');

    pointGroup
      .circle(Config.errorIcon.outer.size)
      .addClass('outer-circle')
      .center(line.cx(), line.cy());

    pointGroup
      .circle(Config.errorIcon.inner.size)
      .addClass('inner-circle')
      .center(line.cx(), line.cy());

    pointGroup
      .text('!')
      .addClass('symbol')
      .center(line.cx(), line.cy());

    const hoverFilter = function (this: G, add: Filter) {
      const blur = add.offset(0, 0).gaussianBlur(Config.errorIcon.blur, Config.errorIcon.blur);
      add.blend(add.$source, blur, 'normal');
      this.size('200%', '200%').move('-50%', '-50%');
    };
    pointGroup.filterWith(hoverFilter);

    group.addClass('error-link');

    return [group, line];
  }

  private addRouter(group: G, line: Line): RouterElement {
    return new RouterElement(
      group,
      { x: line.cx(), y: line.cy() }
    );
  }

  private followMouse() {
    const rootSvg = this._rootGroup.root();
    rootSvg.on('mousemove', (e) => this.moveLink((e as MouseEvent).offsetX, (e as MouseEvent).offsetY));
  }

  private moveLink(x: number, y: number) {
    if (this._lineEl) {
      this._lineEl?.attr('x1', x);
      this._lineEl?.attr('y1', y);
    }
  }

  public remove() {
    this._linkGroup?.remove();

    const rootSvg = this._rootGroup.root();
    rootSvg.off('mousemove');
  }

  public set status(val: LinkStatus) {
    if (this._status !== val) {
      this._linkGroup?.remove();

      switch (val) {
        case LinkStatus.DEPLOYED:
          this.render();
          break;
        case LinkStatus.PENDING:
          this.renderPendingLink();
          break;
        case LinkStatus.ERROR:
          this.renderErrorLink();
          break;
      }

      this._status = val;
    }
  }

  public get center(): Coords {
    return {
      x: this._lineEl?.cx() ?? 0,
      y: this._lineEl?.cy() ?? 0
    };
  }

  public get src(): NodeElement | RouterElement {
    return this._src;
  }

  public get dst(): NodeElement | RouterElement | undefined {
    return this._dst;
  }

  public get rootGroup(): G {
    return this._rootGroup;
  }

  public get linkGroup(): G | undefined {
    return this._linkGroup;
  }

  public get router(): RouterElement | undefined {
    return this._router;
  }

  public get status(): LinkStatus {
    return this._status;
  }

  public get settings(): LinkSettings | undefined {
    return this._settings;
  }
}