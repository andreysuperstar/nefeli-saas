import { SVG } from '@svgdotjs/svg.js';
import { Coords, ResourceType } from './elements';
import { LinkElement, LinkStatus } from './link.element';
import { NodeElement, NodeElementState } from './node.element';
import { RouterElement } from './router.element';

describe('LinkElement', () => {
  const svg = SVG().addTo('body').size('100%', '100%');

  const createNode = (
    label = 'My Label',
    description = 'My Description',
    nodePosition = { x: 100, y: 200 }
  ): NodeElement => {
    return new NodeElement(svg,
      {
        label,
        description,
        type: ResourceType.AZURE
      },
      nodePosition,
      NodeElementState.DEPLOYED);
  };

  it('should render', () => {
    const node = createNode();
    const routerPosition: Coords = { x: 200, y: 300 };
    const router = new RouterElement(svg, routerPosition);

    const link = new LinkElement(svg.group(), node, router);
    const line: SVGLineElement | null = link.rootGroup.node.querySelector('.link > line');

    expect(line?.x1.baseVal.value).toBe(node.svg.cx() + node.svg.bbox().cx);
    expect(line?.y1.baseVal.value).toBe(node.svg.cy() + node.svg.bbox().cy);
    expect(line?.x2.baseVal.value).toBe(router.svg.cx() + router.svg.bbox().cx);
    expect(line?.y2.baseVal.value).toBe(router.svg.cy() + router.svg.bbox().cy);
  });

  it('should display router', () => {
    const node1 = createNode();
    const node2 = createNode();
    const link = new LinkElement(svg.group(), node1, node2, { showRouter: true });
    expect(link.linkGroup?.node.querySelectorAll('.router').length).toBe(1);
  });

  it('should follow mouse if no destination node', () => {
    const link = new LinkElement(svg.group(), createNode());
    expect(link['_lineEl']?.bbox().x).toBe(108);
    expect(link['_lineEl']?.bbox().y).toBe(208);

    const clientX = 300;
    const clientY = 400;
    const ev = new MouseEvent('mousemove', { clientX, clientY });

    link.rootGroup?.root().dispatchEvent(ev);
    // TODO(eric): figure out why below fails intermittently
    // expect(link['_lineEl']?.bbox().x2).toBe(clientX);
    // expect(link['_lineEl']?.bbox().y2).toBe(clientY);
  });

  it('should remove link', () => {
    const node1 = createNode();
    const node2 = createNode();
    const link = new LinkElement(svg.group(), node1, node2);
    let line: SVGLineElement | null = link.rootGroup.node.querySelector('.link > line');

    expect(line).toBeDefined();
    link.remove();
    line = link.rootGroup.node.querySelector('.link > line');
    expect(line).toBeNull();
  });

  it('should get center of line', () => {
    const node1 = createNode(undefined, undefined, { x: 100, y: 100 });
    const node2 = createNode(undefined, undefined, { x: 200, y: 200 });
    const link = new LinkElement(svg.group(), node1, node2);
    expect(link.center).toEqual({ x: 158, y: 158 });
  });

  it('should render different link states', () => {
    const node1 = createNode(undefined, undefined, { x: 100, y: 100 });
    const node2 = createNode(undefined, undefined, { x: 200, y: 200 });
    const link = new LinkElement(svg.group(), node1, node2);

    // verify default link state
    expect(link.linkGroup?.node.getAttribute('class')).toBe('link');

    // verify pending link state
    link.status = LinkStatus.PENDING;
    expect(link.linkGroup?.node.getAttribute('class')).toBe('pending-link');

    // verify error link state
    link.status = LinkStatus.ERROR;
    expect(link.linkGroup?.node.getAttribute('class')).toBe('error-link');

    // verify deployed link state
    link.status = LinkStatus.DEPLOYED;
    expect(link.linkGroup?.node.getAttribute('class')).toBe('link');
  });
});
