import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResourceMenuComponent } from './resource-menu.component';
import { MatCardModule } from '@angular/material/card';
import { MatListModule } from '@angular/material/list';
import { Networks as mockNetworks } from '../../../../mock/data/networks';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { ResourceMenuItemComponent } from '../resource-menu-item/resource-menu-item.component';
import { ESpecNetwork } from '../../../../rest_client/saas/model/eSpecNetwork';

describe('ResourceMenuComponent', () => {
  let component: ResourceMenuComponent;
  let fixture: ComponentFixture<ResourceMenuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        MatCardModule,
        MatListModule
      ],
      declarations: [
        ResourceMenuComponent,
        ResourceMenuItemComponent
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResourceMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render resources', () => {
    const resources: ESpecNetwork[] = Object.values(mockNetworks);
    component.resources = resources;
    fixture.detectChanges();

    const titleDe = fixture.debugElement.query(By.css('mat-card-title'));
    expect(titleDe).not.toBeNull();
    expect(titleDe.nativeElement.classList).toContain('mat-card-title');
    expect(titleDe.nativeElement.textContent).toEqual('Resources');

    const resourcesDes = fixture.debugElement.queryAll(By.css('saas-resource-menu-item'));
    expect(resourcesDes.length).toEqual(resources.length);
    resourcesDes.forEach((resourcesDe: DebugElement, ind: number) => {
      expect(resourcesDe.componentInstance.resource).toEqual(resources[ind]);
    });
  });
});
