import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ESpecNetwork } from '../../../../rest_client/saas/model/eSpecNetwork';

@Component({
  selector: 'saas-resource-menu',
  templateUrl: './resource-menu.component.html',
  styleUrls: ['./resource-menu.component.less']
})
export class ResourceMenuComponent {

  private _resources: ESpecNetwork[] = [];
  private _handleDragStart = new EventEmitter<ESpecNetwork>();

  @Output()
  public get handleDragStart(): EventEmitter<ESpecNetwork> {
    return this._handleDragStart;
  }

  @Input()
  public set resources(value: ESpecNetwork[]) {
    this._resources = value;
  }

  public onDragStart(event: ESpecNetwork) {
    this._handleDragStart.emit(event);
  }

  public get resources(): ESpecNetwork[] {
    return this._resources;
  }
}
