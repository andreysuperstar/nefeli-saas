import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResourceMenuItemComponent } from './resource-menu-item.component';
import { MatListModule } from '@angular/material/list';
import { Networks as mockNetworks } from '../../../../mock/data/networks';
import { ESpecNetwork } from '../../../../rest_client/saas/model/eSpecNetwork';
import { By } from '@angular/platform-browser';


describe('ResourceMenuItemComponent', () => {
  let component: ResourceMenuItemComponent;
  let fixture: ComponentFixture<ResourceMenuItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ MatListModule ],
      declarations: [ ResourceMenuItemComponent ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResourceMenuItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render aws resource', () => {
    const awsResource: ESpecNetwork | undefined = mockNetworks?.networks?.find( r => r.aws);
    expect(awsResource).toBeTruthy();
    component.resource = awsResource;
    fixture.detectChanges();

    const imgDe = fixture.debugElement.query(By.css('img'));
    expect(imgDe).not.toBeNull();
    const imgEl = imgDe.nativeElement as HTMLImageElement;
    expect(imgEl.classList).toContain('mat-list-icon');
    expect(imgEl.src).toContain('assets/aws.png');

    const nameDe = fixture.debugElement.query(By.css('h6'));
    expect(nameDe).not.toBeNull();
    expect(nameDe.nativeElement.classList).toContain('mat-line');
    expect(nameDe.nativeElement.textContent).toEqual(awsResource?.name);

    const regionDe = fixture.debugElement.query(By.css('p'));
    expect(regionDe).not.toBeNull();
    expect(regionDe.nativeElement.classList).toContain('mat-line');
    expect(regionDe.nativeElement.textContent).toEqual(awsResource?.aws?.region);
  });

  it('should render azure resource', () => {
    const azureResource: ESpecNetwork | undefined = mockNetworks?.networks?.find( r => r.azure);
    expect(azureResource).toBeTruthy();
    component.resource = azureResource;
    fixture.detectChanges();

    const imgDe = fixture.debugElement.query(By.css('img'));
    expect(imgDe).not.toBeNull();
    const imgEl = imgDe.nativeElement as HTMLImageElement;
    expect(imgEl.classList).toContain('mat-list-icon');
    expect(imgEl.src).toContain('assets/azure.svg');

    const nameDe = fixture.debugElement.query(By.css('h6'));
    expect(nameDe).not.toBeNull();
    expect(nameDe.nativeElement.classList).toContain('mat-line');
    expect(nameDe.nativeElement.textContent).toEqual(azureResource?.name);

    const regionDe = fixture.debugElement.query(By.css('p'));
    expect(regionDe).not.toBeNull();
    expect(regionDe.nativeElement.classList).toContain('mat-line');
    expect(regionDe.nativeElement.textContent).toEqual(azureResource?.azure?.region);
  });

  // TODO: enable after including of gcp supporting
  xit('should render gcp resource', () => {
    const gcpResource: ESpecNetwork | undefined = Object.values(mockNetworks).find( r => r.gcp);
    expect(gcpResource).toBeTruthy();
    component.resource = gcpResource;
    fixture.detectChanges();

    const imgDe = fixture.debugElement.query(By.css('img'));
    expect(imgDe).not.toBeNull();
    const imgEl = imgDe.nativeElement as HTMLImageElement;
    expect(imgEl.classList).toContain('mat-list-icon');
    expect(imgEl.src).toContain('assets/gcp.png');

    const nameDe = fixture.debugElement.query(By.css('h6'));
    expect(nameDe).not.toBeNull();
    expect(nameDe.nativeElement.classList).toContain('mat-line');
    expect(nameDe.nativeElement.textContent).toEqual(gcpResource?.name);

    const regionDe = fixture.debugElement.query(By.css('p'));
    expect(regionDe).not.toBeNull();
    expect(regionDe.nativeElement.classList).toContain('mat-line');
    expect(regionDe.nativeElement.textContent).toEqual('usa-west1-a');
  });
});
