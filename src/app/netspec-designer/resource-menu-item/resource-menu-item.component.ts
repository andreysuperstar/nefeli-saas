import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ResourceType } from '../../netspec/elements/elements';
import { ESpecNetwork } from '../../../../rest_client/saas/model/eSpecNetwork';

@Component({
  selector: 'saas-resource-menu-item',
  templateUrl: './resource-menu-item.component.html',
  styleUrls: ['./resource-menu-item.component.less']
})
export class ResourceMenuItemComponent {

  private _resource: ESpecNetwork | undefined;
  private _handleDragStart = new EventEmitter<ESpecNetwork>();

  @Input()
  public set resource(value: ESpecNetwork | undefined) {
    this._resource = value;
  }

  @Output()
  public get handleDragStart(): EventEmitter<ESpecNetwork> {
    return this._handleDragStart;
  }

  public get name(): string | undefined {
    return this._resource?.name;
  }

  public get region(): string | undefined {
    switch (this.type) {
      case ResourceType.AWS: return this._resource?.aws?.region;
      case ResourceType.AZURE: return this._resource?.azure?.region;
      // TODO: set the correct GCP region after api updates
      case ResourceType.GCP: return 'usa-west1-a';
    }
    return undefined;
  }

  public get resource(): ESpecNetwork | undefined {
    return this._resource;
  }

  public get type(): ResourceType | undefined {
    if (this._resource?.aws) {
      return ResourceType.AWS;
    }
    if (this._resource?.azure) {
      return ResourceType.AZURE;
    }
    if (this._resource?.gcp) {
      return ResourceType.GCP;
    }
    return undefined;
  }

  public onDragStart() {
    if (this._resource) {
      this._handleDragStart.emit(this._resource);
    }
  }

  public get ResourceType(): typeof ResourceType {
    return ResourceType;
  }
}
