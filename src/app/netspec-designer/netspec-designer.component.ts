import { Component, OnInit, ViewChild } from '@angular/core';
import { NetspecComponent } from '../netspec/netspec.component';
import { NetworksService } from '../../../rest_client/saas/api/networks.service';
import { ESpecNetwork } from '../../../rest_client/saas/model/eSpecNetwork';
import { MatDialog } from '@angular/material/dialog';
import { ApproveNetspecDialogComponent } from './approve-netspec-dialog/approve-netspec-dialog.component';
import { NetspecService } from '../netspec/netspec.service';
import { ExternalNetworkSpec } from '../../../rest_client/saas/model/externalNetworkSpec';
import { Router } from '@angular/router';
import { ESpecNetworks } from '../../../rest_client/saas/model/eSpecNetworks';

@Component({
  selector: 'saas-netspec-designer',
  templateUrl: './netspec-designer.component.html',
  styleUrls: ['./netspec-designer.component.less']
})
export class NetspecDesignerComponent implements OnInit {
  @ViewChild('netspec')
  private _netspecComponent?: NetspecComponent;
  private _resources: ESpecNetwork[] = [];

  constructor(
    private _dialog: MatDialog,
    private _netspecService: NetspecService,
    private _networksService: NetworksService,
    private _router: Router
  ) { }

  public ngOnInit(): void {
    this._networksService
      .getNetworks()
      .subscribe(( { networks }: ESpecNetworks) => {
        this._resources = networks ?? [];
      });
  }

  public onDragStart(resource: ESpecNetwork) {
    if (this._netspecComponent) {
      this._netspecComponent.draggedResource = resource;
    }
  }

  public onApply() {
    const netspec = this._netspecComponent?.netspec;

    if (!netspec?.config) {
      // TODO(eric): display error
      return;
    }

    this._netspecService.applyNetspec(netspec.config).subscribe({
      next: (spec: ExternalNetworkSpec) => {
        const dialogRef = this._dialog.open(ApproveNetspecDialogComponent, {
          ...{
            width: '60%',
            disableClose: true
          },
          data: {
            resourceCosts: spec.status?.resourceCosts
          }
        });

        dialogRef.afterClosed().subscribe({
          next: (success: boolean) => {
            if (success) {
              const netspecId = spec.config?.identifier;

              if (!netspecId) {
                // TODO(eric): display error
                return;
              }

              this._netspecService.approveNetspec(netspecId, '').subscribe({
                next: () => this._router.navigate(['netspec', netspecId])
              });
            }
          }
        });
      }
    });
  }

  public get resources(): ESpecNetwork[] {
    return this._resources;
  }
}
