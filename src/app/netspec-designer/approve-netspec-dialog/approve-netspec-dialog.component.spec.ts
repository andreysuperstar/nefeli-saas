import { HarnessLoader } from '@angular/cdk/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatButtonHarness } from '@angular/material/button/testing';
import { MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatDividerModule } from '@angular/material/divider';
import { Netspec } from '../../../../mock/data/netspecs';

import { ApproveNetspecDialogComponent } from './approve-netspec-dialog.component';

describe('ApproveNetspecDialogComponent', () => {
  let component: ApproveNetspecDialogComponent;
  let fixture: ComponentFixture<ApproveNetspecDialogComponent>;
  let loader: HarnessLoader;
  const resourceCosts = Netspec.status?.resourceCosts;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        MatDialogModule,
        MatDividerModule
      ],
      declarations: [ApproveNetspecDialogComponent],
      providers: [
        {
          provide: MatDialogRef,
          useValue: jasmine.createSpyObj<MatDialogRef<ApproveNetspecDialogComponent>>(
            'MatDialogRef',
            ['close']
          )
        },
        {
          provide: MAT_DIALOG_DATA,
          useValue: {
            resourceCosts
          }
        },
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ApproveNetspecDialogComponent);
    component = fixture.componentInstance;
    loader = TestbedHarnessEnvironment.loader(fixture);
    fixture.detectChanges();
  });

  it('should render costs', async () => {
    expect(component).toBeTruthy();
    const el = fixture.debugElement.nativeNode;

    expect(el.querySelector('.mat-dialog-title').textContent).toBe('Deploying the following resources:');

    const [cancelButton, deployButton] = await loader.getAllHarnesses(MatButtonHarness);
    expect(await cancelButton.getText()).toBe('Cancel');
    expect(await deployButton.getText()).toBe('Deploy');

    const rows = el.querySelectorAll('mat-dialog-content div[fxlayout="row"]');

    expect(rows[0].querySelectorAll('.header')[0].textContent)
      .toBe(resourceCosts?.['one'].monthlyEstimate?.description);
    expect(rows[0].querySelectorAll('.header')[1].textContent)
      .toBe(`${resourceCosts?.['one'].monthlyEstimate?.cost ?? ''} ${resourceCosts?.['one'].monthlyEstimate?.currency ?? ''}`);
    expect(rows[1].querySelectorAll('div')[0].textContent)
      .toBe(`› ${resourceCosts?.['one'].prices?.[0].unit ?? ''}`);
    expect(rows[1].querySelectorAll('div')[1].textContent)
      .toBe(`${resourceCosts?.['one'].prices?.[0].unitPrice ?? ''} ${resourceCosts?.['one'].prices?.[0].currency ?? ''}`);
    expect(rows[2].querySelectorAll('div')[0].textContent)
      .toBe(`› ${resourceCosts?.['one'].prices?.[1].unit ?? ''}`);
    expect(rows[2].querySelectorAll('div')[1].textContent)
      .toBe(`${resourceCosts?.['one'].prices?.[1].unitPrice ?? ''} ${resourceCosts?.['one'].prices?.[1].currency ?? ''}`);
    expect(rows[3].querySelectorAll('div')[0].textContent)
      .toBe(`› ${resourceCosts?.['one'].prices?.[2].unit ?? ''}`);
    expect(rows[3].querySelectorAll('div')[1].textContent)
      .toBe(`${resourceCosts?.['one'].prices?.[2].unitPrice ?? ''} ${resourceCosts?.['one'].prices?.[2].currency ?? ''}`);
    expect(rows[4].querySelectorAll('.header')[0].textContent)
      .toBe(resourceCosts?.['three'].monthlyEstimate?.description);
    expect(rows[4].querySelectorAll('.header')[1].textContent)
      .toBe(`${resourceCosts?.['three'].monthlyEstimate?.cost ?? ''} ${resourceCosts?.['three'].monthlyEstimate?.currency ?? ''}`);
    expect(rows[5].querySelectorAll('div')[0].textContent)
      .toBe(`› ${resourceCosts?.['three'].prices?.[0].unit ?? ''}`);
    expect(rows[5].querySelectorAll('div')[1].textContent)
      .toBe(`${resourceCosts?.['three'].prices?.[0].unitPrice ?? ''} ${resourceCosts?.['three'].prices?.[0].currency ?? ''}`);
    expect(rows[6].querySelectorAll('.header')[0].textContent)
      .toBe(resourceCosts?.['two'].monthlyEstimate?.description);
    expect(rows[6].querySelectorAll('.header')[1].textContent)
      .toBe(`${resourceCosts?.['two'].monthlyEstimate?.cost ?? ''} ${resourceCosts?.['two'].monthlyEstimate?.currency ?? ''}`);
    expect(rows[7].querySelectorAll('div')[0].textContent)
      .toBe(`› ${resourceCosts?.['two'].prices?.[0].unit ?? ''}`);
    expect(rows[7].querySelectorAll('div')[1].textContent)
      .toBe(`${resourceCosts?.['two'].prices?.[0].unitPrice ?? ''} ${resourceCosts?.['two'].prices?.[0].currency ?? ''}`);
    expect(rows[8].querySelectorAll('div')[0].textContent)
      .toBe(`› ${resourceCosts?.['two'].prices?.[1].unit ?? ''}`);
    expect(rows[8].querySelectorAll('div')[1].textContent)
      .toBe(`${resourceCosts?.['two'].prices?.[1].unitPrice ?? ''} ${resourceCosts?.['two'].prices?.[1].currency ?? ''}`);
    expect(rows[9].querySelectorAll('div')[0].textContent)
      .toBe(`› ${resourceCosts?.['two'].prices?.[2].unit ?? ''}`);
    expect(rows[9].querySelectorAll('div')[1].textContent)
      .toBe(`${resourceCosts?.['two'].prices?.[2].unitPrice ?? ''} ${resourceCosts?.['two'].prices?.[2].currency ?? ''}`);
  });

  it('should close dialog on approve', async () => {
    const [cancelButton, deployButton] = await loader.getAllHarnesses(MatButtonHarness);
    expect(cancelButton).toBeDefined();
    await deployButton.click();
    expect(component['_dialogRef'].close).toHaveBeenCalledWith(true);
  });
});
