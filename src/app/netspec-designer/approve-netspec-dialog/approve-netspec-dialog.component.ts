import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ExternalNetworkSpecStatusResourceCost } from '../../../../rest_client/saas/model/externalNetworkSpecStatusResourceCost';

type ResourceCosts = { [key: string]: ExternalNetworkSpecStatusResourceCost; };
interface ApproveNetspecDialogData {
  resourceCosts?: ResourceCosts;
}
@Component({
  selector: 'saas-approve-netspec-dialog',
  templateUrl: './approve-netspec-dialog.component.html',
  styleUrls: ['./approve-netspec-dialog.component.less']
})
export class ApproveNetspecDialogComponent {
  private _resourceCosts?: ResourceCosts;

  constructor(
    private _dialogRef: MatDialogRef<ApproveNetspecDialogComponent>,
    @Inject(MAT_DIALOG_DATA) private _data: ApproveNetspecDialogData
  ) {
    this._resourceCosts = this._data.resourceCosts;
  }

  public onApprove() {
    this._dialogRef.close(true);
  }

  public get resourceCosts(): ResourceCosts | undefined {
    return this._resourceCosts;
  }
}
