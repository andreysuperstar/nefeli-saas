import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ResourceMenuItemComponent } from './resource-menu-item/resource-menu-item.component';
import { ResourceMenuComponent } from './resource-menu/resource-menu.component';
import { MatListModule } from '@angular/material/list';
import { NetspecDesignerComponent } from './netspec-designer.component';
import { MatSidenavModule } from '@angular/material/sidenav';
import { NetspecModule } from '../netspec/netspec.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatCardModule } from '@angular/material/card';
import { NetspecDesignerRoutingModule } from './netspec-designer-routing.module';
import { MatButtonModule } from '@angular/material/button';
import { ApproveNetspecDialogComponent } from './approve-netspec-dialog/approve-netspec-dialog.component';
import { MatDialogModule } from '@angular/material/dialog';

@NgModule({
  declarations: [
    ResourceMenuItemComponent,
    ResourceMenuComponent,
    NetspecDesignerComponent,
    ApproveNetspecDialogComponent
  ],
  imports: [
    CommonModule,
    FlexLayoutModule,
    MatListModule,
    MatSidenavModule,
    NetspecModule,
    MatCardModule,
    MatButtonModule,
    NetspecDesignerRoutingModule,
    MatDialogModule
  ],
  exports: [NetspecDesignerComponent]
})
export class NetspecDesignerModule { }
