import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NetspecDesignerComponent } from './netspec-designer.component';
import { ResourceMenuComponent } from './resource-menu/resource-menu.component';
import { ResourceMenuItemComponent } from './resource-menu-item/resource-menu-item.component';
import { MatCardModule } from '@angular/material/card';
import { MatListModule } from '@angular/material/list';
import { MatSidenavModule } from '@angular/material/sidenav';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { Networks as mockNetworks } from '../../../mock/data/networks';
import { By } from '@angular/platform-browser';
import { NetspecComponent } from '../netspec/netspec.component';
import { ESpecNetworks } from '../../../rest_client/saas/model/eSpecNetworks';
import { ESpecNetwork } from '../../../rest_client/saas/model/eSpecNetwork';
import { MatDialogModule } from '@angular/material/dialog';
import { RouterTestingModule } from '@angular/router/testing';
import { HarnessLoader } from '@angular/cdk/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { MatButtonHarness } from '@angular/material/button/testing';
import { Netspec, NetspecConfig } from '../../../mock/data/netspecs';
import { ApproveNetspecDialogComponent } from './approve-netspec-dialog/approve-netspec-dialog.component';

describe('NetspecDesignerComponent', () => {
  let component: NetspecDesignerComponent;
  let fixture: ComponentFixture<NetspecDesignerComponent>;
  let loader: HarnessLoader;
  let httpTestingController: HttpTestingController;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        BrowserAnimationsModule,
        HttpClientTestingModule,
        MatCardModule,
        MatListModule,
        MatSidenavModule,
        MatDialogModule,
        RouterTestingModule
      ],
      declarations: [
        NetspecDesignerComponent,
        ResourceMenuComponent,
        ResourceMenuItemComponent,
        NetspecComponent,
        ApproveNetspecDialogComponent
      ]
    })
      .compileComponents();
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NetspecDesignerComponent);
    component = fixture.componentInstance;
    loader = TestbedHarnessEnvironment.loader(fixture);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render resources menu', () => {
    const mockResponse: ESpecNetworks = { networks: Object.values(mockNetworks) };
    const req = httpTestingController.expectOne('http://localhost/v1/networks');
    req.flush(mockResponse);
    component.ngOnInit();
    expect(req.request.method).toBe('GET');

    fixture.detectChanges();

    expect(component).toBeTruthy();
    const menuDe = fixture.debugElement.query(By.css('saas-resource-menu'));
    expect(menuDe).not.toBeNull();
    expect(menuDe.componentInstance.resources).toEqual(mockResponse.networks);
  });

  it('should drag resource on canvas', () => {
    const mockResponse: ESpecNetworks = mockNetworks;
    const req = httpTestingController.expectOne('http://localhost/v1/networks');
    req.flush(mockResponse);
    component.ngOnInit();

    const res: ESpecNetwork = mockResponse.networks![0];
    const resourceMenu = fixture.debugElement.query(By.directive(ResourceMenuComponent));
    resourceMenu.componentInstance.onDragStart(res);
    component['_netspecComponent']?.onDrop({
      clientX: 200,
      clientY: 300
    } as any);

    const el = fixture.debugElement.nativeNode;
    expect(el.querySelector('svg .node .label').textContent).toBe(res.name);
    expect(el.querySelector('svg .node .description').textContent).toBe(res.aws?.region);
  });

  it('should apply new netspec', async () => {
    const resourceMenu = fixture.debugElement.query(By.directive(ResourceMenuComponent));
    const mockResources: ESpecNetwork[] = mockNetworks?.networks ?? [];

    resourceMenu.componentInstance.onDragStart(mockResources[0]);
    component['_netspecComponent']?.onDrop({
      clientX: 200,
      clientY: 300
    } as any);

    resourceMenu.componentInstance.onDragStart(mockResources[1]);
    component['_netspecComponent']?.onDrop({
      clientX: 300,
      clientY: 400
    } as any);

    const networkNodes = fixture.debugElement.nativeNode.querySelectorAll('svg > svg');
    networkNodes[0].dispatchEvent(new Event('click'));
    networkNodes[1].dispatchEvent(new Event('click'));

    const applyButton = loader.getHarness(MatButtonHarness.with({
      text: 'Apply netspec'
    }))
    await (await applyButton).click();

    const createReq = httpTestingController.expectOne('http://localhost/v1/netspecs');
    expect(createReq.request.method).toBe('POST');
    expect(createReq.request.body).toEqual({
      name: 'Netspec #1',
      version: '1',
      networkIds: ['1', '2'],
      routers: [],
      links: [{
        nodeA: {
          networkIdentifier: '1',
          logicalRouterIdentifier: undefined
        },
        nodeB: {
          networkIdentifier: '2',
          logicalRouterIdentifier: undefined
        }
      }]
    });

    createReq.flush(NetspecConfig);

    const getReq = httpTestingController.expectOne(`http://localhost/v1/netspecs/${NetspecConfig.identifier!}?status=true`);
    expect(getReq.request.method).toBe('GET');
    getReq.flush(Netspec);

    fixture.detectChanges();
    expect(component['_dialog'].openDialogs[0].componentInstance instanceof ApproveNetspecDialogComponent).toBeTrue();
  });
});
