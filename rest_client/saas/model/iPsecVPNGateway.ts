/**
 * SaaS API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
import { IPsecVPNGatewayAddressPair } from './iPsecVPNGatewayAddressPair';
import { IPsecVPNGatewayIPAddr } from './iPsecVPNGatewayIPAddr';


/**
 * IPsecVPNGateway is a generic object for a VPN gateway which will serve as one end of an IPsec tunnel.  At the time of writing the following services can be used to implement this.  AWS\'s Virtual Private Gateway   https://docs.aws.amazon.com/vpn/latest/s2svpn/how_it_works.html#VPNGateway Azure\'s VPN Gateway and Virtual Network Gateway:   https://docs.microsoft.com/en-us/azure/vpn-gateway/vpn-gateway-about-vpngateways GCP\'s Cloud VPN Gateway:   https://cloud.google.com/compute/docs/reference/rest/v1/targetVpnGateways  NOTE(melvin): CPC should keep in mind that GCP\'s Cloud VPN must be deployed alongside their Cloud Router service.
 */
export interface IPsecVPNGateway { 
    /**
     * Let the provider chose an address for us. This is useful when using service\'s like Azure\'s Virtual Network Gateway, which we have no control over the addresses of at config-time. In situations like this, we\'ll let them generate one for us and tuck it into state that a peer IPsecVPNGateway can access.
     */
    autoAddr?: object;
    /**
     * Let the provider chose two addresses for us. This is useful when using service\'s like Azure\'s VPN Gateway, which we have no control over the addresses of at config-time. In situations like this, we\'ll let them generate one for us and tuck it into state that a peer IPsecVPNGateway can access.
     */
    autoAddrHa?: object;
    /**
     * The BGP ASN to be assigned to this VPN gateway. No two IPsecVPNGateways with a connection between them should share an ASN or BPG will not work. All VPNateways originating in an Azure VWAN Hub (VPCHub) will be forced to use the Azure default ASN 65515 and as a result all VPN connections between Azure VWAN Hubs will be forced to use static routing, not BGP.
     */
    bgpAsn?: string;
    peerFixedAddr?: IPsecVPNGatewayIPAddr;
    peerFixedAddrHa?: IPsecVPNGatewayAddressPair;
    /**
     * Let the provider chose a pair of addresses for us. This is useful when using service\'s like AWS\'s S2S VPN Connection, which we have no control over the public IP addresses of at config-time. In situations like this, we\'ll let them generate one for us and tuck it into state that a peer IPsecVPNGateway can access.
     */
    perConnection?: object;
    publicFixedAddr?: IPsecVPNGatewayIPAddr;
    publicFixedAddrHa?: IPsecVPNGatewayAddressPair;
    /**
     * In the case of AWS or GCP based IPsec tunnels, a /30 APIPA subnet will be allocated to each tunnel along with a public IP address and the local and remote BGP peering addresses will be accessible via the terraform state of the connection object. For example, the local peer address is accessible via a single VPN tunnel\'s vgw address and the local address can be referenced as a tunnel\'s cgw address.
     */
    useConnection?: object;
    /**
     * The UUID of the VPCHub resource that this gateway is associated with. If this Gateway is associated with a network rather than a VPCHub then this should remain unset.
     */
    vpcHubId?: string;
}

