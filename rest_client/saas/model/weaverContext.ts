/**
 * SaaS API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


export interface WeaverContext { 
    /**
     * The identifier of the corresponding netspec network object.
     */
    networkId?: string;
    /**
     * The identifier of the corresponding Nefeli Provider object.
     */
    providerId?: string;
    /**
     * The Weaver site ID.
     */
    siteId?: string;
    /**
     * The Weaver tenant ID.
     */
    tenantId?: string;
}

