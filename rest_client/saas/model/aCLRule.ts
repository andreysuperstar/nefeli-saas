/**
 * SaaS API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
import { Explanation } from './explanation';
import { ACLFlowMatch } from './aCLFlowMatch';
import { ACLAction } from './aCLAction';


/**
 * AWS:     Network ACLs:         https://docs.aws.amazon.com/vpc/latest/userguide/vpc-network-acls.html         (note that return traffic must be enabled explicitly, i.e.,         VPC Network ACLs are not stateful) Azure:     Azure Firewall:         https://docs.microsoft.com/en-us/azure/firewall/overview     TODO(mtovino): Do we need to deal with Private Endpoints         https://docs.microsoft.com/en-us/azure/private-link/private-endpoint-overview         which appear to be the closest Azure analog to AWS VPC Network ACL? GCP:     TODO(mtovino): Are firewall rules stateful?     https://cloud.google.com/vpc/docs/vpc#firewall_rules
 */
export interface ACLRule { 
    action?: ACLAction;
    explanation?: Explanation;
    flow?: ACLFlowMatch;
}

