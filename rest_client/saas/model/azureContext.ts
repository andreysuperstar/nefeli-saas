/**
 * SaaS API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


export interface AzureContext { 
    /**
     * The identifier of the corresponding netspec network object.
     */
    networkId?: string;
    /**
     * The identifier of the corresponding Nefeli Provider object.
     */
    providerId?: string;
    /**
     * The name assigned to the Vnet by the cloud provider.
     */
    providerVnetName?: string;
    region?: string;
    /**
     * The existing resource group associated with/to associate with this network.
     */
    resourceGroupName?: string;
}

